<!--
  #%L
  JAXX :: Widgets Gis
  %%
  Copyright (C) 2008 - 2017 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as 
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  -->
<JPanel id='editorPanel' layout='{new BorderLayout()}' onFocusGained='editor.requestFocus()'>
  <import>
    org.nuiton.jaxx.widgets.gis.DmsCoordinate

    java.io.Serializable
    java.awt.BorderLayout
    javax.swing.JFormattedTextField
  </import>

  <style source="CoordinateEditor.jcss"/>

  <!-- show reset property -->
  <Boolean id='showReset' javaBean='false'/>

  <!-- show reset tip -->
  <String id='showResetTip' javaBean=''/>

  <!-- model -->
  <SignedDmsCoordinateEditorModel id='model'/>

  <script><![CDATA[
public void init(boolean longitudeEditor) { handler.init(longitudeEditor); }
public void setBean(Serializable bean) { model.setBean(bean); }
public void setPropertySign(String property ) { model.setPropertySign(property); }
public void setPropertyDegree(String property ) { model.setPropertyDegree(property); }
public void setPropertyMinute(String property ) { model.setPropertyMinute(property); }
public void setPropertySecond(String property ) { model.setPropertySecond(property); }
public void setDisplayZeroWhenNull(boolean displayZeroWhenNull) { handler.setDisplayZeroWhenNull(displayZeroWhenNull); }
public void setFillWithZero(boolean fillWithZero) { handler.setFillWithZero(fillWithZero); }
public void setValue(DmsCoordinate value) { handler.setValue(value, true); }
]]>
  </script>

  <JToolBar id='toolbarLeft' constraints='BorderLayout.WEST' styleClass="enabled">
    <JButton id='resetButton' onActionPerformed='handler.resetEditor()'/>
  </JToolBar>
  <JFormattedTextField id='editor' constraints='BorderLayout.CENTER' styleClass="enabled"
                       onKeyReleased='handler.onKeyReleased(event)'/>
</JPanel>
