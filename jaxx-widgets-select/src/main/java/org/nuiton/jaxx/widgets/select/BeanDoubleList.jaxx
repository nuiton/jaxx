<!--
  #%L
  JAXX :: Widgets Select
  %%
  Copyright (C) 2008 - 2017 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as 
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  -->
<Table genericType='O' implements='org.nuiton.jaxx.runtime.bean.BeanTypeAware&lt;O&gt;'>

  <import>
    org.nuiton.decorator.JXPathDecorator
  </import>

  <!-- filterable list property -->
  <Boolean id='filterable' javaBean='true'/>

  <!-- flag to reverse the sort -->
  <Boolean id='reverseSort' javaBean='false'/>

  <!-- show decorator property -->
  <Boolean id='showDecorator' javaBean='true'/>

  <!-- show reset property -->
  <Boolean id='showReset' javaBean='true'/>

  <!-- to be able to select a same entry more than one time property -->
  <Boolean id='useMultiSelect' javaBean='false'/>

  <!-- show showSelectPopupEnabled property -->
  <Boolean id='showSelectPopupEnabled' javaBean='true'/>

  <!-- show highlightFilterText property -->
  <Boolean id='highlightFilterText' javaBean='false'/>

  <!-- bean type -->
  <Class id='beanType' genericType='O' javaBean='null'/>

  <!-- bean property linked state -->
  <String id='property' javaBean='""'/>

  <!-- bean property -->
  <Object id='bean' javaBean='null'/>

  <String id='i18nPrefix' javaBean='"beanlist.common."'/>

  <!-- model -->
  <BeanDoubleListModel id='model' genericType='O' javaBean='new BeanDoubleListModel&lt;O&gt;()'/>

  <!-- sort index property -->
  <Integer id='index' javaBean='0'/>

  <!-- model of sorted property -->
  <ButtonGroup id='indexes'
               onStateChanged='setIndex((Integer)indexes.getSelectedValue())'/>

  <ButtonGroup id='sortGroup'
               onStateChanged='setReverseSort((Boolean)sortGroup.getSelectedValue())'/>

  <!-- popup to change sorted property-->
  <JPopupMenu id='popup'
              onPopupMenuWillBecomeInvisible='getChangeDecorator().setSelected(false)'
              onPopupMenuCanceled='getChangeDecorator().setSelected(false)'>
    <JLabel id='popupSortLabel'/>

    <JRadioButtonMenuItem id='sortUp'/>

    <JRadioButtonMenuItem id='sortDown'/>

    <JSeparator id='popupSeparator'/>
    <JLabel id='popupLabel'/>
    <JSeparator/>
  </JPopupMenu>

  <!-- popup on the selected list -->
  <JPopupMenu id='selectedListPopup'>
    <JMenuItem id='selectedListMoveUpAction'
               onActionPerformed='handler.moveUpSelected( (O) selectedList.getSelectedValue())'/>
    <JMenuItem id='selectedListMoveDownAction'
               onActionPerformed='handler.moveDownSelected( (O) selectedList.getSelectedValue())'/>
    <JMenuItem id='selectedListRemoveAction'
               onActionPerformed='model.removeFromSelected( (O) selectedList.getSelectedValue())'/>
  </JPopupMenu>

  <row>
    <cell columns='3' fill='both'>
      <JPanel layout='{new BorderLayout()}' id='beforeFilterPanel'/>
    </cell>
  </row>
  <row>
    <cell columns='3' fill='both'>
      <JPanel layout='{new BorderLayout()}' id='filterPanel'>
        <JPanel layout='{new BorderLayout()}' constraints='BorderLayout.WEST'>
          <JLabel id='filterFieldLabel' constraints='BorderLayout.WEST'/>
          <JToolBar id='toolbarLeft' constraints='BorderLayout.EAST'>
            <JButton id='resetButton' onActionPerformed='filterField.setText("")'/>
          </JToolBar>
        </JPanel>
        <JTextField id='filterField' constraints='BorderLayout.CENTER'/>
        <JToolBar id='toolbarRight' constraints='BorderLayout.EAST'>
          <JToggleButton id='changeDecorator'
                         onActionPerformed='getHandler().togglePopup()'/>
        </JToolBar>
      </JPanel>
    </cell>
  </row>
  <row>
    <cell weightx='0.5' weighty='1' fill='both'>
      <JScrollPane onFocusGained='universeList.requestFocus()'>
        <!-- List of all the remaining available elements -->
        <JList id='universeList' genericType='O'
               onFocusGained='handler.selectFirstRowIfNoSelection(event)'
               onMouseClicked='handler.onUniverseListClicked(event)'
               onKeyPressed='handler.onKeyPressedOnUniverseList(event)'/>
      </JScrollPane>
    </cell>

    <cell anchor='north'>
      <JPanel layout='{new GridLayout(0,1)}'>
        <JButton id='addButton' onActionPerformed='handler.select()'/>
        <JButton id='removeButton' onActionPerformed='handler.unselect()'/>
      </JPanel>
    </cell>

    <cell weightx='0.5' weighty='1' fill='both'>
      <JScrollPane onFocusGained='selectedList.requestFocus()'>
        <!-- List of the selected elements -->
        <JList id='selectedList' genericType='O'
               onFocusGained='handler.selectFirstRowIfNoSelection(event)'
               onMouseClicked='handler.onSelectedListClicked(event)'
               onKeyPressed='handler.onKeyPressedOnSelectedList(event)'/>
      </JScrollPane>
    </cell>
  </row>

  <script><![CDATA[

public void init(JXPathDecorator<O> decorator, JXPathDecorator<O> decorator2, List<O> universe, List<O> selected) {
      handler.init(decorator, decorator2, universe, selected);
  }

public void init(JXPathDecorator<O> decorator, List<O> universe, List<O> selected) {
    handler.init(decorator, universe, selected);
}

]]></script>

</Table>
