/*
 * #%L
 * JAXX :: Widgets
 * %%
 * Copyright (C) 2008 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
#indexes {
  useToolTipText:true;
}

#sortGroup {
  useToolTipText:true;
  selectedValue:{isReverseSort()};
}

#popup {
  border:{ new TitledBorder(t("beancombobox.popup.title")) };
}

#popupSortLabel {
  actionIcon:"bean-sort";
  text:"bean.sort.label";
}

#sortUp {
  buttonGroup:"sortGroup";
  value:{false};
  actionIcon:"bean-sort-up";
  text:"bean.sort.up";
  selected:{!isReverseSort()};
}

#sortDown {
  buttonGroup:"sortGroup";
  value:{true};
  actionIcon:"bean-sort-down";
  text:"bean.sort.down";
  selected:{isReverseSort()};
}

#toolbarLeft {
  floatable:false;
  borderPainted:false;
  visible:{isShowReset()};
}

#resetButton {
  actionIcon:"combobox-reset";
  toolTipText:"beancombobox.action.reset.tip";
  focusable:false;
  focusPainted:false;
  enabled:{isEditable() && isEnabled()};
}

#combobox {
  model: {new JaxxFilterableComboBoxModel<O>()};
  selectedItem:{getSelectedItem()};
  enabled:{isEnabled()};
  editable:{isEditable()};
  focusable:{isEnabled() && isEditable()};
  maximumRowCount: { getMaximumRowCount() != null ? getMaximumRowCount() : 8 };
}

#toolbarRight {
  floatable:false;
  borderPainted:false;
  visible:{isShowDecorator()};
}

#changeDecorator {
  actionIcon:"combobox-sort";
  toolTipText:"beancombobox.action.sort.tip";
  focusable:false;
  focusPainted:false;
  enabled:{isShowDecorator() && isEnabled()};
}



