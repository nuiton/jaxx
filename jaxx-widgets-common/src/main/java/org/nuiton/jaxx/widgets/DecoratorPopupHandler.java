package org.nuiton.jaxx.widgets;

/*
 * #%L
 * JAXX :: Widgets Common
 * %%
 * Copyright (C) 2008 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.decorator.MultiJXPathDecorator;
import org.nuiton.jaxx.runtime.swing.JAXXButtonGroup;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JSeparator;
import javax.swing.SwingUtilities;
import java.awt.Dimension;
import java.beans.Introspector;

import static org.nuiton.i18n.I18n.n;
import static org.nuiton.i18n.I18n.t;

/**
 * Created on 11/30/14.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.18
 */
public abstract class DecoratorPopupHandler implements Runnable {

    /** Logger. */
    public static final Log log = LogFactory.getLog(DecoratorPopupHandler.class);

    public static final String DEFAULT_POPUP_LABEL = n("bean.popup.label");

    public static final String DEFAULT_SELECTED_TOOLTIP = n("bean.sort.on");

    public static final String DEFAULT_NOT_SELECTED_TOOLTIP = n("bean.sort.off");

    public abstract JPopupMenu getPopup();

    public abstract JComponent getInvoker();

    @Override
    public void run() {

        updatePopup();

        Dimension dim = getPopup().getPreferredSize();

        JComponent invoker = getInvoker();
        getPopup().show(
                invoker,
                (int) (invoker.getPreferredSize().getWidth() - dim.getWidth()),
                invoker.getHeight()
        );
    }

    /** Toggle the popup visible state. */
    public void togglePopup() {
        boolean newValue = !getPopup().isVisible();

        if (log.isTraceEnabled()) {
            log.trace(newValue);
        }

        if (!newValue) {
            if (getPopup() != null) {
                getPopup().setVisible(false);
            }
            return;
        }
        SwingUtilities.invokeLater(this);
    }

    protected void updatePopup() {
        getPopup().pack();
    }

    /**
     * Creation de l'ui pour modifier le décorateur.
     *
     * @param selectedTip
     * @param notSelectedTip
     * @param i18nPrefix
     * @param title
     * @param indexes
     * @param popupLabel
     * @param sortUp
     * @param sortDown
     * @param decorator      le decorateur a utiliser
     */
    public void preparePopup(String selectedTip,
                             String notSelectedTip,
                             String i18nPrefix,
                             String title,
                             ButtonGroup indexes,
                             JSeparator popupSeparator,
                             JLabel popupLabel,
                             AbstractButton sortUp,
                             AbstractButton sortDown,
                             MultiJXPathDecorator<?> decorator) {
        if (selectedTip == null) {
            // use default selected tip text
            selectedTip = DEFAULT_SELECTED_TOOLTIP;
        }
        if (notSelectedTip == null) {
            // use default selected tip text
            notSelectedTip = DEFAULT_NOT_SELECTED_TOOLTIP;
        }
        JPopupMenu popup = getPopup();

        //Container container = ui.getIndexesContainer();

        int nbContext = decorator.getNbContext();
        if (nbContext > 1) {
            for (int i = 0; i < nbContext; i++) {
                String property = i18nPrefix + decorator.getProperty(i);
                String propertyI18n = t(property);
                JRadioButtonMenuItem button = new JRadioButtonMenuItem(propertyI18n);
                button.putClientProperty(JAXXButtonGroup.BUTTON8GROUP_CLIENT_PROPERTY, indexes);
                button.putClientProperty(JAXXButtonGroup.VALUE_CLIENT_PROPERTY, i);
                popup.add(button);
                if (selectedTip != null) {
                    button.putClientProperty(JAXXButtonGroup.SELECTED_TIP_CLIENT_PROPERTY, t(selectedTip, propertyI18n));
                }
                if (notSelectedTip != null) {
                    button.putClientProperty(JAXXButtonGroup.NOT_SELECTED_TIP_CLIENT_PROPERTY, t(notSelectedTip, propertyI18n));
                }
                button.setSelected(false);
                indexes.add(button);
            }
        }
        if (title == null) {
            // use default popup title
            title = DEFAULT_POPUP_LABEL;

            Class<?> type = decorator.getType();
            String beanI18nKey;
            if (type == null) {
                beanI18nKey = n("bean.unknown.type");
            } else {
                beanI18nKey = i18nPrefix + Introspector.decapitalize(type.getSimpleName());
            }
            String beanI18n = t(beanI18nKey);
            title = t(title, beanI18n);
        } else {
            title = t(title);
        }

        sortDown.putClientProperty(JAXXButtonGroup.SELECTED_TIP_CLIENT_PROPERTY, t("bean.sort.down.tip"));
        sortDown.putClientProperty(JAXXButtonGroup.NOT_SELECTED_TIP_CLIENT_PROPERTY, t("bean.sort.down.toSelect.tip"));

        sortUp.putClientProperty(JAXXButtonGroup.SELECTED_TIP_CLIENT_PROPERTY, t("bean.sort.up.tip"));
        sortUp.putClientProperty(JAXXButtonGroup.NOT_SELECTED_TIP_CLIENT_PROPERTY, t("bean.sort.up.toSelect.tip"));

        if (nbContext < 2) {
            getPopup().remove(popupSeparator);
            getPopup().remove(popupLabel);
        }
        popupLabel.setText(title);
        getPopup().setLabel(title);
        getPopup().invalidate();
    }
}
