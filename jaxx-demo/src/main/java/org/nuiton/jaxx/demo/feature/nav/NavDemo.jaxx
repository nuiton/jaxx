<!--
  #%L
  JAXX :: Demo
  %%
  Copyright (C) 2008 - 2017 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as 
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  -->

<org.nuiton.jaxx.demo.DemoPanel layout='{new BorderLayout()}'>

  <import>
    org.nuiton.jaxx.demo.feature.nav.tree.NavDemoTreeHelper
    org.nuiton.jaxx.demo.feature.nav.treetable.NavDemoTreeTableHelper
    org.jdesktop.swingx.JXTreeTable
    org.nuiton.jaxx.demo.entities.DemoDataProvider
  </import>

  <CardLayout2 id='contentLayout'/>

  <DemoDataProvider id='dataProvider'/>

  <NavDemoTreeHelper id='treeHelper' constructorParams='getDataProvider()'/>

  <NavDemoTreeTableHelper id='treeTableHelper'
                          constructorParams='getDataProvider()'/>

  <script><![CDATA[

@Override
protected String[] getSources() {
    return addDefaultSources(
            "tree/NavDemoTreeNode.java",
            "tree/NavDemoTreeHelper.java",
            "tree/NavDemoTreeCellRenderer.java",
            "tree/ActorsTreeNodeLoador.java",
            "tree/MoviesTreeNodeLoador.java",
            "treetable/NavDemoTreeTableNode.java",
            "treetable/NavDemoTreeTableHelper.java",
            "treetable/ActorsTreeTableNodeLoador.java",
            "treetable/MoviesTreeTableNodeLoador.java"
     );
}
 ]]>
  </script>

  <JSplitPane id='splitPane'
              constraints='BorderLayout.CENTER'
              oneTouchExpandable='true'>

    <JTabbedPane>
      <tab title='jaxxdemo.tree.tabtitle'>
        <JScrollPane border='{null}'
                     horizontalScrollBarPolicy='{JScrollPane.HORIZONTAL_SCROLLBAR_NEVER}'
                     verticalScrollBarPolicy='{JScrollPane.VERTICAL_SCROLLBAR_NEVER}'>

          <JTree id="navigationTree"
                 font-size='11'
                 rootVisible='false'
                 showsRootHandles='false'
                 model='{treeHelper.createModel()}'/>

        </JScrollPane>
      </tab>
      <tab title='jaxxdemo.treeTable.tabtitle'>
        <JScrollPane border='{null}'
                     horizontalScrollBarPolicy='{JScrollPane.HORIZONTAL_SCROLLBAR_NEVER}'
                     verticalScrollBarPolicy='{JScrollPane.VERTICAL_SCROLLBAR_NEVER}'>

          <JXTreeTable id="navigationTreeTable"
                       font-size='11'
                       rootVisible='false'
                       showsRootHandles='false'
                       columnControlVisible='true'
                       treeTableModel='{treeTableHelper.createModel()}'/>

        </JScrollPane>
      </tab>
    </JTabbedPane>

    <JPanel id="content" layout="{contentLayout}"/>

  </JSplitPane>

</org.nuiton.jaxx.demo.DemoPanel>
