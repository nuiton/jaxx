/*
 * #%L
 * JAXX :: Demo
 * %%
 * Copyright (C) 2008 - 2014 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

JToolBar {
    borderPainted:false;
    floatable:false;
    opaque:false;
}

JSplitPane {
    oneTouchExpandable:true;
    continuousLayout:true;
    dividerSize:6;
}

#mainFrame {
    title:"JAXX Demo";
    iconImage:{SwingUtil.createIcon(getConfig().getIconPath()).getImage()};
    undecorated:{getConfig().isFullScreen()};
}

#menu {
    _help:{"ui.main.menu"};
}

#menuFile {
    text:"jaxxdemo.menu.file";
    i18nMnemonic: "jaxxdemo.menu.file.mnemonic";
    _help:{"ui.main.menu.file"};
}

#menuFileConfiguration {
    text:"jaxxdemo.action.configuration";
    toolTipText:"jaxxdemo.action.configuration.tip";
    actionIcon:"config";
    i18nMnemonic: "jaxxdemo.action.configuration.mnemonic";
    _help:{"ui.main.menu.file.configuration"};
}

#menuFileLanguage {
    text:"jaxxdemo.menu.file.locale";
    toolTipText:"jaxxdemo.menu.file.locale";
    actionIcon:"translate";
    i18nMnemonic: "jaxxdemo.menu.file.locale.mnemonic";
    _help:{"ui.main.menu.file.locale"};
}


#menuFileLanguageFR {
    text:"jaxxdemo.action.locale.fr";
    toolTipText:"jaxxdemo.action.locale.fr.tip";
    actionIcon:"i18n-fr";
    enabled:{!acceptLocale(getConfig().getLocale(), "fr_FR")};
    i18nMnemonic: "jaxxdemo.action.locale.fr.mnemonic";
    _help:{"ui.main.menu.file.locale.fr"};
}

#menuFileLanguageUK {
    text:"jaxxdemo.action.locale.uk";
    toolTipText:"jaxxdemo.action.locale.uk.tip";
    actionIcon:"i18n-uk";
    enabled:{!acceptLocale(getConfig().getLocale(), "en_GB")};
    i18nMnemonic: "jaxxdemo.action.locale.uk.mnemonic";
    _help:{"ui.main.menu.file.locale.uk"};
}

#menuFileFullscreen {
    text:"jaxxdemo.action.fullscreen";
    toolTipText:"jaxxdemo.action.fullscreen.tip";
    actionIcon:"fullscreen";
    i18nMnemonic: "jaxxdemo.action.fullscreen.mnemonic";
    visible:{!isUndecorated()};
    _help:{"ui.main.menu.file.fullscreen"};
}

#menuFileNormalscreen {
    text:"jaxxdemo.action.normalscreen";
    toolTipText:"jaxxdemo.action.normalscreen.tip";
    actionIcon:"leave-fullscreen";
  i18nMnemonic: "jaxxdemo.action.normalscreen.mnemonic";
    visible:{isUndecorated()};
    _help:{"ui.main.menu.file.leave-fullscreen"};
}

#menuFileExit {
    text:"jaxxdemo.action.exit";
    toolTipText:"jaxxdemo.action.exit.tip";
    actionIcon:"exit";
    i18nMnemonic: "jaxxdemo.action.exit.mnemonic";
    _help:{"ui.main.menu.file.exit"};
}

#menuHelp {
    text:"jaxxdemo.menu.help";
    i18nMnemonic: "jaxxdemo.menu.help.mnemonic";
    _help:{"ui.main.menu.help"};
}

#menuHelpLogs {
    text:"jaxxdemo.action.showLogs";
    i18nMnemonic: "jaxxdemo.action.showLogs.mnemonic";
    _help:{"ui.main.action.showLogs"};
}

#menuHelpHelp {
    text:"jaxxdemo.action.help";
    toolTipText:"jaxxdemo.action.help.tip";
    actionIcon:"help";
    i18nMnemonic: "jaxxdemo.action.help.mnemonic";
    _help:{"ui.main.menu.help.help"};
}

#menuHelpSite {
    text:"jaxxdemo.action.site";
    toolTipText:"jaxxdemo.action.site.tip";
    actionIcon:"site";
    i18nMnemonic: "jaxxdemo.action.site.mnemonic";
    _help:{"ui.main.menu.help.site"};
}

#menuHelpAbout {
    text:"jaxxdemo.action.about";
    toolTipText:"jaxxdemo.action.about.tip";
    actionIcon:"about";
    i18nMnemonic: "jaxxdemo.action.about.mnemonic";
    _help:{"ui.main.menu.help.about"};
}

#showHelp {
    toolTipText:"jaxxdemo.action.showHelp.tip";
    actionIcon:"show-help";
    borderPainted:false;
    visible:true;
}

#navigationPane {
    border:{null};
    minimumSize:{new Dimension(230,0)};
    horizontalScrollBarPolicy:{JScrollPane.HORIZONTAL_SCROLLBAR_NEVER};
}

#navigation {
    rootVisible:false;
    showsRootHandles:false;
    largeModel:true;
    font-size:11;
    model:{getTreeHelper().createModel()};
    cellRenderer:{new DemoCellRenderer(getTreeHelper().getDataProvider())};
}

#splitpane {
    orientation:{JSplitPane.HORIZONTAL_SPLIT};
    _help:{"ui.main.body.db"};
    resizeWeight:1.0;
}

#contentLayout {
    useOnlyVisibleComponentDimension:true;
}

#content {
    layout:{contentLayout};
    _help:{"ui.main.body.db.view.content"};
}
/*
#toolbar {
    layout:{new BoxLayout(toolbar, 0)};
    _help:{"ui.main.toolbar"};
}*/
