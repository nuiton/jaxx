<!--
  #%L
  JAXX :: Widgets Font
  %%
  Copyright (C) 2008 - 2017 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as 
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  -->
<JPanel layout='{new BorderLayout()}'>

  <!-- default size of font -->
  <Float id='defaultFontSize' javaBean='12f'/>

  <!-- size of font -->
  <Float id='fontSize' javaBean='null'/>

  <Boolean id='showDefaultFontSize' javaBean='true'/>

  <Boolean id='showFontSize' javaBean='false'/>

  <script><![CDATA[
public void init() {
    handler.init();
}

boolean updateDefaultSizeEnabled(Float fontSize, Float defaultFontSize, boolean enabled) {
   return handler.updateDefaultSizeEnabled( fontSize,defaultFontSize, enabled);
}

public void setCallBack(Runnable action) {
    handler.setCallBack(action);
}
]]>
  </script>
  <JToolBar floatable='false'
            borderPainted='false'
            opaque='{isOpaque()}'
            constraints='BorderLayout.CENTER'>

    <JButton id='downSize'
             actionIcon='font-size-down'
             toolTipText='fontsize.action.down.tip'
             focusable='false'
             focusPainted='false'
             enabled='{isEnabled()}'
             onActionPerformed='setFontSize(fontSize - 1)'/>

    <JButton id='defaultSize'
             actionIcon='font-size'
             toolTipText='fontsize.action.default.tip'
             focusable='false'
             focusPainted='false'
             visible='{isShowDefaultFontSize()}'
             enabled='{updateDefaultSizeEnabled(getFontSize(), getDefaultFontSize(), isEnabled())}'
             onActionPerformed='setFontSize(defaultFontSize)'/>
    <JButton id='upSize'
             actionIcon='font-size-up'
             toolTipText='fontsize.action.up.tip'
             focusable='false'
             focusPainted='false'
             enabled='{isEnabled()}'
             onActionPerformed='setFontSize(fontSize + 1)'/>

    <JLabel visible='{isShowFontSize()}'
            text='{SwingUtil.getStringValue(getFontSize())}'/>
  </JToolBar>

</JPanel>
