<!--
  #%L
  JAXX :: Widgets Status
  %%
  Copyright (C) 2008 - 2017 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as 
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  -->

<JLabel implements='java.awt.event.ActionListener'
        foreground='{Color.BLACK}'
        background='{Color.WHITE}'>

  <Integer id='delay' javaBean='60000'/>

  <String id='pattern' javaBean='"HH:mm"'/>

  <javax.swing.Timer id='timer' delay='{delay}' constructorParams='delay,this'/>

  <import>
    java.awt.Color
    java.awt.event.ActionEvent
    java.text.SimpleDateFormat
    java.text.DateFormat
    java.util.Date
    java.beans.PropertyChangeEvent
    java.beans.PropertyChangeListener
  </import>
  <script><![CDATA[

protected void $afterCompleteSetup() {

    addPropertyChangeListener(PROPERTY_DELAY, new PropertyChangeListener() {

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            timer = new Timer((Integer) evt.getNewValue(), ClockWidget.this);
        }
    });
}

@Override
public void actionPerformed(ActionEvent evt) {
    update();
}

/** Adds a feature to the Notify attribute of the Clock object */
@Override
public void addNotify() {
    super.addNotify();
    update();
    timer.start();
}

@Override
public void removeNotify() {
    timer.stop();
    super.removeNotify();
}

protected void update() {
    DateFormat format = new SimpleDateFormat(pattern);
    setText(format.format(new Date()));
}
]]>
  </script>
</JLabel>
