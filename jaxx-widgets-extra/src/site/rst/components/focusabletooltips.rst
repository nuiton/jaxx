.. -
.. * #%L
.. * JAXX :: Extra Widgets
.. * %%
.. * Copyright (C) 2004 - 2017 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

Focusables tooltips
===================

Focusable tooltips are eclipse-like tooltips original taken from `RSyntaxTextArea`_
project.

The nuiton-widget version also include modification to allow putting focusable
tooltips on non-``JTextArea``, ``JComponent``.

Screenshots
-----------

Default tooltip:

.. image:: ../images/components/focusablett1.jpg

Focused tooltip:

.. image:: ../images/components/focusablett2.jpg

Using
-----

To use focusable tooltip, you need to redefine the ``getTooltipText()`` method
on related components ::

  JTable table = new JTable(tableModel) {
    /** Single tooltip instance. */
    protected FocusableTip focusableTip;
  
    @Override
    public String getToolTipText(MouseEvent e) {
        String text = super.getToolTipText(e);
        if (focusableTip == null) {
            focusableTip = new FocusableTip(this, null);
        }
        focusableTip.toolTipRequested(e, text);

        return null;
    }
  };


Sources
-------

  * `Fifesoft blog`_.

.. _RSyntaxTextArea: http://fifesoft.com/rsyntaxtextarea/
.. _Fifesoft blog: http://fifesoft.com/blog/?p=93
