/*
 * #%L
 * JAXX :: Extra Widgets
 * %%
 * Copyright (C) 2004 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.widgets.extra.tree;

import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * Filter tree model.
 *
 * Take a delegate {@link TreeModel} filter it with {@link TreeFilter}.
 *
 * @author chatellier
 *
 *         Based on : http://forums.sun.com/thread.jspa?forumID=57&amp;threadID=5378510
 */
public class FilterTreeModel implements TreeModel {

    /** Listener for data and structure change notification. */
    protected final Collection<TreeModelListener> treeModelListeners;

    /** Real application {@link TreeModel}. */
    protected final TreeModel delegateModel;

    /** Filter to use (can be null : no filtering). */
    protected TreeFilter treeFilter;

    /**
     * Constructor with delegate model.
     *
     * @param delegateModel delegate data model
     */
    public FilterTreeModel(TreeModel delegateModel) {
        this(delegateModel, null);
    }

    /**
     * Constructor with delegate model.
     *
     * @param delegateModel
     * @param filter
     */
    public FilterTreeModel(TreeModel delegateModel, TreeFilter filter) {
        this.delegateModel = delegateModel;
        this.treeFilter = filter;
        treeModelListeners = new ArrayList<>();
    }

    /**
     * Change filter.
     *
     * Send a {@code treeStructureChanged} event on all registred listeners.
     *
     * @param treeFilter new filter
     */
    public void setFilter(TreeFilter treeFilter) {
        this.treeFilter = treeFilter;
        TreePath path = new TreePath(delegateModel.getRoot());
        fireTreeStructureChanged(path);
    }

    /**
     * Send a {@code treeStructureChanged} event on all registred listeners.
     *
     * @param path new path to send in notification
     */
    protected void fireTreeStructureChanged(TreePath path) {
        TreeModelEvent event = new TreeModelEvent(delegateModel, path);
        for (TreeModelListener current : treeModelListeners) {
            current.treeStructureChanged(event);
        }
    }

    @Override
    public int getChildCount(Object parent) {
        int realCount = delegateModel.getChildCount(parent);
        int filterCount = 0;

        for (int i = 0; i < realCount; i++) {
            Object child = delegateModel.getChild(parent, i);
            // null filter allowed, no filtering
            if (treeFilter == null || treeFilter.include(delegateModel, child)) {
                filterCount++;
            }
        }
        return filterCount;
    }

    @Override
    public Object getChild(Object parent, int index) {
        int cnt = -1;
        for (int i = 0; i < delegateModel.getChildCount(parent); i++) {
            Object child = delegateModel.getChild(parent, i);
            // null filter allowed, no filtering
            if (treeFilter == null || treeFilter.include(delegateModel, child)) {
                cnt++;
            }
            if (cnt == index) {
                return child;
            }
        }
        return null;
    }

    /*
     * @see javax.swing.tree.TreeModel#getRoot()
     */
    @Override
    public Object getRoot() {
        return delegateModel.getRoot();
    }

    /*
     * @see javax.swing.tree.TreeModel#isLeaf(java.lang.Object)
     */
    @Override
    public boolean isLeaf(Object node) {
        return delegateModel.isLeaf(node);
    }

    /*
     * @see javax.swing.tree.TreeModel#valueForPathChanged(javax.swing.tree.TreePath, java.lang.Object)
     */
    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {
        delegateModel.valueForPathChanged(path, newValue);
    }

    /*
     * @see javax.swing.tree.TreeModel#getIndexOfChild(java.lang.Object, java.lang.Object)
     */
    @Override
    public int getIndexOfChild(Object parent, Object child) {
        return delegateModel.getIndexOfChild(parent, child);
    }

    /*
     * @see javax.swing.tree.TreeModel#addTreeModelListener(javax.swing.event.TreeModelListener)
     */
    @Override
    public void addTreeModelListener(TreeModelListener l) {
        delegateModel.addTreeModelListener(l);
        treeModelListeners.add(l);
    }

    /*
     * @see javax.swing.tree.TreeModel#removeTreeModelListener(javax.swing.event.TreeModelListener)
     */
    @Override
    public void removeTreeModelListener(TreeModelListener l) {
        delegateModel.removeTreeModelListener(l);
        treeModelListeners.remove(l);
    }
}
