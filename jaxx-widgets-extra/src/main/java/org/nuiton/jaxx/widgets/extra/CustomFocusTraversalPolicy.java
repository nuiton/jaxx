/*
 * #%L
 * JAXX :: Extra Widgets
 * %%
 * Copyright (C) 2004 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
 
 /*
 * Created on 16 mai 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.nuiton.jaxx.widgets.extra;

import java.awt.Component;
import java.awt.Container;
import java.awt.FocusTraversalPolicy;
import java.util.List;

/**
 * @author cedric
 *
 *         TODO To change the template for this generated type comment go to
 *         Window - Preferences - Java - Code Style - Code Templates
 */
public class CustomFocusTraversalPolicy extends FocusTraversalPolicy {

    protected List<?> order = null;

    public CustomFocusTraversalPolicy(List<?> order) {
        this.order = order;
    }

    @Override
    public Component getFirstComponent(Container focusCycleRoot) {
        if (order.size() != 0) {
            return (Component) order.get(0);
        }
        return null;
    }

    @Override
    public Component getLastComponent(Container focusCycleRoot) {
        if (order.size() > 0) {
            return (Component) order.get(order.size() - 1);
        }
        return null;
    }

    @Override
    public Component getComponentAfter(Container focusCycleRoot,
                                       Component aComponent) {
        int index = order.indexOf(aComponent);
        if (index != -1) {
            Component nextC = (Component) order.get((index + 1) % order.size());
            if (nextC.isEnabled()) {
                return nextC;
            }
            return getComponentAfter(focusCycleRoot, nextC);
        }
        return null;
    }

    @Override
    public Component getComponentBefore(Container focusCycleRoot,
                                        Component aComponent) {
        int index = order.indexOf(aComponent);
        if (index != -1) {
            Component nextC = (Component) order.get((index - 1 + order.size())
                                                            % order.size());
            if (nextC.isEnabled()) {
                return nextC;
            }
            return getComponentBefore(focusCycleRoot, nextC);
        }
        return null;
    }

    @Override
    public Component getDefaultComponent(Container focusCycleRoot) {
        if (order.size() > 0) {
            return (Component) order.get(0);
        }
        return null;
    }

}
