/*
 * #%L
 * JAXX :: Extra Widgets
 * %%
 * Copyright (C) 2004 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.widgets.extra;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.Resource;
import org.xml.sax.SAXException;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.LayoutManager;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Map;

/**
 * XMLGridLayout uses a XML input (a string, for the time being) to define the
 * layout of components inside the container. To do this a set of constraints in
 * the form of GridBagConstraints are build and associated to a name. When
 * adding the component to the container, the name of the constraint to be used
 * must be supplied, using the add(name,component) method. If the name
 * associated to the component has no associated constrain, the component is not
 * layout. <br>
 * Implementation Note: Internally this class uses a GridBagLayout and delegates
 * to it all the calculations necesary to layout the components.
 *
 * @author Rafael Alvarez
 */
// TODO: put a test in place... Just in case
public class XMLGridLayout implements LayoutManager {
    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private final Log log = LogFactory.getLog(XMLGridLayout.class);

    private Map constraints;
    private GridBagLayout layout;
    private String layoutString;

    public XMLGridLayout(String layoutStringOrFile) {

        /*
         * // looking for file on filesystem first File file = new
         * File(layoutStringOrFile); URL url = null; if(file.exists()){ try{ url
         * = file.toURL(); }catch(MalformedURLException eee){ throw new
         * RuntimeException("File exists but not convertible to URL: " + file,
         * eee); } } else { // looking for file in classpath url =
         * getClass().getResource(layoutStringOrFile); }
         */
        URL url = Resource.getURLOrNull(layoutStringOrFile);
        log.debug("ask: " + layoutStringOrFile + " url: " + url);
        if (url != null) {
            // file found
            init(url);
        } else {
            // no file found
            init(layoutStringOrFile);
        }
    }

    public XMLGridLayout(URL url) {
        init(url);
    }

    protected void init(URL url) {
        StringBuilder sb = new StringBuilder();
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(url
                                                                                 .openStream()));
            int c = -1;
            while ((c = in.read()) != -1) {
                sb.append((char) c);
            }
        } catch (IOException eee) {
            throw new RuntimeException("Error when reading file: " + url, eee);
        }
        init(sb.toString());
    }

    /**
     * @param layoutString string that represente the layout
     */
    protected void init(String layoutString) {
        // long initime = System.currentTimeMillis();
        this.layout = new GridBagLayout();
        this.layoutString = layoutString;
        XMLGridParseConstraints parser = null;
        try {
            parser = new XMLGridParseConstraints();
        } catch (SAXException e) {
            throw new java.lang.RuntimeException(e);
        }

        try {
            constraints = parser.parse(layoutString);
        } catch (SAXException e) {
            System.err.println(parser.getErrors());
            throw new RuntimeException("Invalid layout string: " + layoutString);
        }
        // System.out.println("XMLGridLayout Initialization Elapsed Time:"
        // +(System.currentTimeMillis() - initime) + "ms");
    }

    /**
     * Binds a name in the layout to the specified component. If the name is not
     * defined in the layout an error message will be send to System.err
     *
     * @param name The name in the layout to bind the component to
     * @param comp The component to bind to the specified name.
     */
    @Override
    public void addLayoutComponent(String name, Component comp) {
        GridBagConstraints constraint = (GridBagConstraints) constraints
                .get(name);
        if (constraint != null) {
            layout.addLayoutComponent(comp, constraint);
        } else {
            System.err
                    .println("(XMLGridLayout) No constraint defined for name "
                                     + name);
        }
    }

    @Override
    public void removeLayoutComponent(Component comp) {
        layout.removeLayoutComponent(comp);
    }

    @Override
    public Dimension preferredLayoutSize(Container parent) {
        return layout.preferredLayoutSize(parent);
    }

    @Override
    public Dimension minimumLayoutSize(Container parent) {
        return layout.minimumLayoutSize(parent);
    }

    @Override
    public void layoutContainer(Container parent) {
        // long initime = System.currentTimeMillis();
        layout.layoutContainer(parent);
        // System.out.println("XMLGridLayout Layout Elapsed Time:"
        // +(System.currentTimeMillis() - initime) + "ms");

    }

    public GridBagConstraints getConstraint(String componentName) {
        return (GridBagConstraints) constraints.get(componentName);
    }

    public String getLayoutString() {
        return this.layoutString;
    }
}
