/*
 * #%L
 * JAXX :: Extra Widgets
 * %%
 * Copyright (C) 2004 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/* *
 * SplashScreen.java
 *
 * Created: Jul 29, 2004
 *
 * @author Cédric Pineau <pineau@codelutin.com>
 * @version $Revision$
 *
 * Last update : $Date$
 * by : $Author$
 */

package org.nuiton.jaxx.widgets.extra;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.border.EtchedBorder;
import java.awt.AlphaComposite;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.Image;
import java.awt.Point;
import java.net.URL;
import java.util.Timer;
import java.util.TimerTask;

/**
 * @author kmorin - morin@codelutin.com
 */
public class SplashScreen extends JFrame {

    private static final long serialVersionUID = 1L;

//    public final static long DEFAULT_DURATION = 3000;

    public final static long DEFAULT_REMANENCE = 5000;

    public final static int DEFAULT_STEPPING_LEFT_INSET = 100;

    public final static int DEFAULT_STEPPING_RIGHT_INSET = 100;

    public final static int DEFAULT_STEPPING_BOTTOM_INSET = 100;

    public final static Composite defaultComposite = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.7f);

    protected long remanence = DEFAULT_REMANENCE;

    protected Image splashImage;

    protected final Title applicationTitle;

//    protected String[] stepDescriptions;

    protected Image[] stepImages;

    protected int currentStep = -1;

    protected int steppingLeftInset = DEFAULT_STEPPING_LEFT_INSET;

    protected int steppingRightInset = DEFAULT_STEPPING_RIGHT_INSET;

    protected int steppingBottomInset = DEFAULT_STEPPING_BOTTOM_INSET;

    protected Image currentImage;

    protected Composite currentComposite;

    protected ImageAnimator imageAnimator;

    public SplashScreen(String splashImagePath) {
        this(splashImagePath, null);
    }

    public SplashScreen(String splashImagePath, Title applicationTitle) {
        this(splashImagePath, applicationTitle, null);
    }

    public SplashScreen(String splashImagePath, Title applicationTitle, String[] stepImagesPaths) {
        super();
        this.applicationTitle = applicationTitle;
//        this.stepDescriptions = stepDescriptions;
        initialize(splashImagePath, stepImagesPaths);
    }

    /**
     * @return Returns the stepDescriptions.
     */
//    public String[] getStepDescriptions() {
//        return stepDescriptions;
//    }

    /**
     * @param stepDescriptions The stepDescriptions to set.
     */
//    public void setStepDescriptions(String[] stepDescriptions) {
//        this.stepDescriptions = stepDescriptions;
//    }

    /**
     * @return Returns the steppingInset.
     */
    public int getSteppingLeftInset() {
        return steppingLeftInset;
    }

    /**
     * @param steppingLeftInset The steppingInset to set.
     */
    public void setSteppingLeftInset(int steppingLeftInset) {
        this.steppingLeftInset = steppingLeftInset;
    }

    /**
     * @return Returns the steppingRightInset.
     */
    public int getSteppingRightInset() {
        return steppingRightInset;
    }

    /**
     * @param steppingRightInset The steppingRightInset to set.
     */
    public void setSteppingRightInset(int steppingRightInset) {
        this.steppingRightInset = steppingRightInset;
    }

    /**
     * @return Returns the applicationTitle.
     */
    public Title getApplicationTitle() {
        return applicationTitle;
    }

    /**
     * @return Returns the currentStep.
     */
    public int getCurrentStep() {
        return currentStep;
    }

    /**
     * @param currentStep The currentStep to set.
     */
    public void setCurrentStep(int currentStep) {
        this.currentStep = currentStep;
    }

    /**
     * @return Returns the estimatedDuration.
     */
//    public long getEstimatedDuration() {
//        return estimatedDuration;
//    }

    /**
     * @return Returns the splashImage.
     */
    public Image getSplashImage() {
        return splashImage;
    }

    /**
     * @return Returns the stepImages.
     */
    public Image[] getStepImages() {
        return stepImages;
    }

    /**
     * @return Returns the steppingBottomInset.
     */
    public int getSteppingBottomInset() {
        return steppingBottomInset;
    }

    /**
     * @param steppingBottomInset The steppingBottomInset to set.
     */
    public void setSteppingBottomInset(int steppingBottomInset) {
        this.steppingBottomInset = steppingBottomInset;
    }

    /**
     * @return Returns the currentComposite.
     */
    public Composite getCurrentComposite() {
        return currentComposite;
    }

    /**
     * @param currentComposite The currentComposite to set.
     */
    public void setCurrentComposite(Composite currentComposite) {
        this.currentComposite = currentComposite;
    }

    public long getRemanence() {
        return remanence;
    }

    public void setRemanence(long remanence) {
        this.remanence = remanence;
    }

    public void nextStep() {
        if (imageAnimator != null) {
            imageAnimator.cancel();
        }
        setCurrentStep(getCurrentStep() + 1);
        currentAlpha = 0.1f;
        currentImage = stepImages[getCurrentStep()];
        imageAnimator = new ImageAnimator();
        new Timer().schedule(imageAnimator, 0, 70);
        repaint();
    }

    public void complete() {
        if (imageAnimator != null) {
            imageAnimator.cancel();
        }
        new Timer().schedule(new FrameHider(), remanence);
        repaint();
    }

    protected float currentAlpha = 0.1f;

    protected boolean alphaUp = true;

    protected void animateImage() {
        if (currentAlpha <= 0.3) {
            alphaUp = true;
        } else if (currentAlpha >= 0.7) {
            alphaUp = false;
        }
        if (alphaUp) {
            currentAlpha += 0.1;
        } else {
            currentAlpha -= 0.1;
        }
        setCurrentComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER,
                                                       currentAlpha));
        repaint();
    }

    protected Image getStepImage(int i) {
        Image image = null;
        if (i == currentStep) {
            image = currentImage;
        } else {
            image = stepImages[i];
        }
        return image;
    }

    /**
     * @param step the required step
     * @return Returns the currentComposite.
     */
    public Composite getCompositeFor(int step) {
        if (step == getCurrentStep()) {
            return getCurrentComposite();
        }
        return defaultComposite;
    }

    protected void paintSplash(Graphics g) {
        g.drawImage(splashImage, 0, 0, null);

        if (applicationTitle != null) {
            g.setColor(applicationTitle.getColor());
            g.setFont(applicationTitle.getFont());
            Point position = applicationTitle.getPosition();
            g.drawString(applicationTitle.getLabel(), position.x, position.y);
        }

        if (stepImages != null) {
            // draw step images
            int steppingInset = (getSplashImage().getWidth(null)
                    - getSteppingLeftInset() - getSteppingRightInset())
                    / (stepImages.length + 1);
            int steppingY = getSplashImage().getHeight(null)
                    - getSteppingBottomInset();
            for (int i = 0; i <= currentStep; i++) {
                Image image = getStepImage(i);
                Graphics2D g2 = (Graphics2D) g;
                Composite oldComposite = g2.getComposite();
                g2.setComposite(getCompositeFor(i));
                g2.drawImage(image, getSteppingLeftInset()
                                     + (steppingInset * (i + 1)) - image.getWidth(null) / 2,
                             steppingY, null);
                g2.setComposite(oldComposite);
            }
        }
    }

    protected void initialize(String splashImagePath, String[] stepIconsPaths) {
        if (stepIconsPaths != null) {
            stepImages = new Image[stepIconsPaths.length];
            for (int i = 0; i < stepIconsPaths.length; i++) {
                stepImages[i] = getImage(stepIconsPaths[i]);
            }
        }

        splashImage = getImage(splashImagePath);

        initializeUI();

        this.setUndecorated(true);
        this.positionAtCenter(splashImage.getWidth(null), splashImage.getHeight(null));
        this.setVisible(true);
    }

    protected Image getImage(String imagePath) {
        Image result = null;
        URL imageURL = getClass().getResource(imagePath);
        if (imageURL != null) {
            result = new ImageIcon(imageURL).getImage();
        }
        return result;
    }

    /**
     * Positions the window at the centre of the screen, taking into account the
     * specified width and height
     */
    protected void positionAtCenter(int width, int height) {
        setBounds(0, 0, width, height);
        setLocationRelativeTo(null);
    }

    protected void initializeUI() {
        this.setContentPane(getJContentPane());
    }

    protected JPanel jContentPane = null;

    protected JPanel getJContentPane() {
        if (jContentPane == null) {
            jContentPane = new JPanel();
            jContentPane.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
            jContentPane.setLayout(new java.awt.GridBagLayout());
            GridBagConstraints gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.insets = new java.awt.Insets(1, 1, 1, 1);
            gridBagConstraints.fill = GridBagConstraints.BOTH;
            gridBagConstraints.weightx = 1.0D;
            gridBagConstraints.weighty = 1.0D;
            jContentPane.add(getSplashPane(), gridBagConstraints);
        }
        return jContentPane;
    }

    protected JPanel splashPane = null;

    protected JPanel getSplashPane() {
        if (splashPane == null) {

            splashPane = new JPanel() {
                private static final long serialVersionUID = 1L;

                @Override
                public void paint(Graphics g) {
                    paintSplash(g);
                }
            };
            splashPane.setLayout(new java.awt.BorderLayout());

            if (stepImages == null) {
                JProgressBar progressBar = new JProgressBar();
                progressBar.setIndeterminate(true);
                splashPane.add(progressBar, BorderLayout.SOUTH);
            }
        }
        return splashPane;
    }

    protected class ImageAnimator extends TimerTask {

        @Override
        public void run() {
            animateImage();
        }

    }

    protected class FrameHider extends TimerTask {

        @Override
        public void run() {
            dispose();
            setVisible(false);
        }

    }

    //

    public static void main(String[] args) {
        Title title = new Title("SplashScreen v0.1\n Released under GNU General Public License v2.0",
                                new Font(Font.SERIF, Font.PLAIN, 20),
                                new Point(20, 20),
                                Color.WHITE);
        SplashScreen f = new SplashScreen("/redGecko.jpg", title, new String[]{
                "/64x64/camera.png", "/64x64/joystick.png",
                "/64x64/modem.png", "/64x64/mouse.png",
                "/64x64/pda.png", "/64x64/printer.png",
                "/64x64/scanner.png", "/64x64/tablet.png"
        });
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < 8; i++) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            f.nextStep();
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        f.complete();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.exit(0);
    }

    public static class Title {

        protected final String label;
        protected final Font font;
        protected final Point position;
        protected final Color color;

        public Title(String label, Font font, Point position, Color color) {
            this.label = label;
            this.font = font;
            this.position = position;
            this.color = color;
        }

        public String getLabel() {
            return label;
        }

        public Font getFont() {
            return font;
        }

        public Point getPosition() {
            return position;
        }

        public Color getColor() {
            return color;
        }
    }

}
