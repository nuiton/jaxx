/*
 * #%L
 * JAXX :: Extra Widgets
 * %%
 * Copyright (C) 2004 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/* *
 * MessageDialog.java
 *
 * Created: 6 avr. 2004
 *
 * @author Benjamin Poussin - poussin@codelutin.com
 */

package org.nuiton.jaxx.widgets.extra;

import org.nuiton.i18n.I18n;

import javax.swing.JFileChooser;
import java.io.File;

public class MessageDialog { // MessageDialog

//    public static void error(String title, String message, Throwable eee) {
//        JOptionPane.showMessageDialog(null, message + "\n"
//                + ExceptionUtil.stackTrace(eee), title,
//                JOptionPane.ERROR_MESSAGE);
//    }

    static protected File currentDirectory = new File(".");

    static public File getLastChoice() {
        return currentDirectory;
    }

    static public void setCurrentDirectory(String dir) {
        currentDirectory = new File(dir);
    }

    /**
     * @return le nom du fichier entre dans la boite de dialogue. Si le bouton
     * annuler est utilisé, ou qu'il y a une erreur retourne null.
     */
    static public String getFile() {
        return getFile(I18n.t("nuitonwidgets.message.file"));
    }

    static public String getFile(String buttonText) {
        try {
            JFileChooser chooser = new JFileChooser(currentDirectory);
            chooser.setDialogType(JFileChooser.CUSTOM_DIALOG);
            chooser.setApproveButtonText(buttonText);
            chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            int returnVal = chooser.showDialog(null, null);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File theFile = chooser.getSelectedFile();
                if (theFile != null) {
                    currentDirectory = theFile;
                    return theFile.getAbsolutePath();
                }
            } else {
                return null;
            }
        } catch (Exception e) {
            java.util.logging.Logger.getLogger(
                    "MessageDialog.getFile").severe(
                    "Erreur:");
        }
        return null;
    }

    /**
     * @return le nom du repertoire entre dans la boite de dialogue. Si le
     * bouton annuler est utilisé, ou qu'il y a une erreur retourne null.
     */
    static public String getDirectory() {
        try {
            JFileChooser chooser = new JFileChooser(currentDirectory);
            chooser.setDialogType(JFileChooser.CUSTOM_DIALOG);
            chooser.setApproveButtonText(I18n.t("nuitonwidgets.message.folder"));
            chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            int returnVal = chooser.showDialog(null, null);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File theFile = chooser.getSelectedFile();
                if (theFile != null) {
                    currentDirectory = theFile;
                    if (theFile.isDirectory()) {
                        return theFile.getAbsolutePath();
                    }
                }
            } else {
                return null;
            }
        } catch (Exception e) {
            java.util.logging.Logger.getLogger(
                    "MessageDialog.getDirectory").severe(
                    "Erreur:");
        }
        return null;
    }

} // MessageDialog

