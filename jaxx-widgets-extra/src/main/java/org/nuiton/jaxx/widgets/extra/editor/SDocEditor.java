/*
 * #%L
 * JAXX :: Extra Widgets
 * %%
 * Copyright (C) 2004 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.widgets.extra.editor;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.FileUtil;
import sdoc.Gutter;
import sdoc.SyntaxSupport;

import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

/**
 * SDocEditor.
 *
 * Created: 19 janv. 07 10:25:18
 *
 * @author poussin
 * @version $Revision$
 *
 *          Last update: $Date$
 *          by : $Author$
 */
public class SDocEditor extends DefaultEditor {

    /** serialVersionUID */
    private static final long serialVersionUID = -7907912891843847963L;

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private final Log log = LogFactory.getLog(SDocEditor.class);

    protected final SyntaxSupport syntaxSupport;

    public SDocEditor() {
        syntaxSupport = SyntaxSupport.getInstance();

        // fix background to white (fix bug in nimbus)
        editor.setBackground(Color.WHITE);

        // if you want line numbering
        scrollPane.setRowHeaderView(new Gutter(editor, scrollPane));
    }

    @Override
    public void setSyntax(Editor.EditorSyntaxConstant editorSyntax) {
        Integer constant = null;
        if (editorSyntax.equals(Editor.EditorSyntaxConstant.SQL)) {
            constant = SyntaxSupport.SQL_LEXER;
        } else if (editorSyntax.equals(Editor.EditorSyntaxConstant.JAVA)) {
            constant = SyntaxSupport.JAVA_LEXER;
        } else if (editorSyntax.equals(Editor.EditorSyntaxConstant.XML)) {
            constant = SyntaxSupport.XML_LEXER;
        } else {
            log.warn("Syntax '" + editorSyntax.getName() + "' is not yet supported by SDocEditor");
        }
        if (constant != null) {
            syntaxSupport.addSupport(constant, editor);
        }
    }

    @Override
    public boolean accept(File file) {
        String ext = FileUtil.extension(file);
        boolean result = "java".equalsIgnoreCase(ext);
        result = result || "xml".equalsIgnoreCase(ext);
        result = result || "sql".equalsIgnoreCase(ext);
        return result;
    }

    @Override
    public boolean accept(Editor.EditorSyntaxConstant editorSyntaxConstant) {
        return editorSyntaxConstant.isSupported(
                Editor.EditorSyntaxConstant.JAVA,
                Editor.EditorSyntaxConstant.XML,
                Editor.EditorSyntaxConstant.SQL,
                Editor.EditorSyntaxConstant.R);
    }

    @Override
    public boolean open(File file) {
        try {
            Document doc = editor.getDocument();
            EditorHelper.removeUndoRedoSupport(editor);
            doc.removeDocumentListener(this);

            Reader in = new BufferedReader(new FileReader(file));
            // editor.read(in, file);
            // String text = editor.getText();

            String text = "";
            char c;
            int last;

            while ((last = in.read()) != -1) {
                c = (char) last;
                // on peut avoir \r\n (windows) \r (macos) \n (unix)
                if ('\r' == c) { // pour windows et macos on remplace par \n
                    in.mark(1);
                    last = in.read();
                    if (last != -1) {
                        if ('\n' != (char) last) {
                            // on a seulement \r on remet le dernier caractere
                            // lu
                            in.reset();
                        }
                        // dans tous les cas \r ou \r\n on remplace par \n
                        c = '\n';
                    }
                }
                text += c;
            }

            String ext = FileUtil.extension(file);
            if ("java".equalsIgnoreCase(ext)) {
                syntaxSupport.addSupport(SyntaxSupport.JAVA_LEXER, editor);
            } else if ("xml".equalsIgnoreCase(ext)) {
                syntaxSupport.addSupport(SyntaxSupport.XML_LEXER, editor);
            } else if ("sql".equalsIgnoreCase(ext)) {
                syntaxSupport.addSupport(SyntaxSupport.SQL_LEXER, editor);
            }

            doc = editor.getDocument();

            try {
                // read(in, doc, 0);
                doc.insertString(0, text, null);
            } catch (BadLocationException eee) {
                if (log.isWarnEnabled()) {
                    log.warn("Can't insert text", eee);
                }
            }
            // editor.setDocument(doc);
            // editor.setText(text);

            doc = editor.getDocument();
            doc.addDocumentListener(this);
            EditorHelper.addUndoRedoSupport(editor);
            isModified = false;
            return true;
        } catch (FileNotFoundException eee) {
            if (log.isWarnEnabled()) {
                log.warn("Can't find file: " + file, eee);
            }
        } catch (IOException eee) {
            if (log.isWarnEnabled()) {
                log.warn("Can't open file: " + file, eee);
            }
        }
        return false;
    }

    /*
     * Inserts content from the given stream, which will be treated as plain
     * text.
     * 
     * @param in The stream to read from
     * @param doc The destination for the insertion.
     * @param pos The location in the document to place the content >= 0.
     * @exception IOException on any I/O error
     * @exception BadLocationException if pos represents an invalid location
     *                within the document.
     *
    private void read(Reader in, Document doc, int pos) throws IOException,
            BadLocationException {

        char[] buff = new char[4096];
        int nch;
        boolean lastWasCR = false;
        boolean isCRLF = false;
        boolean isCR = false;
        int last;
        boolean wasEmpty = (doc.getLength() == 0);
        AttributeSet attr = null;

        // Read in a block at a time, mapping \r\n to \n, as well as single
        // \r's to \n's. If a \r\n is encountered, \r\n will be set as the
        // newline string for the document, if \r is encountered it will
        // be set as the newline character, otherwise the newline property
        // for the document will be removed.
        while ((nch = in.read(buff, 0, buff.length)) != -1) {
            last = 0;
            for (int counter = 0; counter < nch; counter++) {
                switch (buff[counter]) {
                case '\r':
                    if (lastWasCR) {
                        isCR = true;
                        if (counter == 0) {
                            doc.insertString(pos, "\n", attr);
                            pos++;
                        } else {
                            buff[counter - 1] = '\n';
                        }
                    } else {
                        lastWasCR = true;
                    }
                    break;
                case '\n':
                    if (lastWasCR) {
                        if (counter > (last + 1)) {
                            doc.insertString(pos, new String(buff, last,
                                    counter - last - 1), attr);
                            pos += (counter - last - 1);
                        }
                        // else nothing to do, can skip \r, next write will
                        // write \n
                        lastWasCR = false;
                        last = counter;
                        isCRLF = true;
                    }
                    break;
                default:
                    if (lastWasCR) {
                        isCR = true;
                        if (counter == 0) {
                            doc.insertString(pos, "\n", attr);
                            pos++;
                        } else {
                            buff[counter - 1] = '\n';
                        }
                        lastWasCR = false;
                    }
                    break;
                }
            }
            if (last < nch) {
                if (lastWasCR) {
                    if (last < (nch - 1)) {
                        doc.insertString(pos, new String(buff, last, nch - last
                                - 1), attr);
                        pos += (nch - last - 1);
                    }
                } else {
                    doc.insertString(pos, new String(buff, last, nch - last),
                            attr);
                    pos += (nch - last);
                }
            }
        }
        if (lastWasCR) {
            doc.insertString(pos, "\n", attr);
            isCR = true;
        }
        if (wasEmpty) {
            if (isCRLF) {
                doc.putProperty(DefaultEditorKit.EndOfLineStringProperty,
                        "\r\n");
            } else if (isCR) {
                doc.putProperty(DefaultEditorKit.EndOfLineStringProperty, "\r");
            } else {
                doc.putProperty(DefaultEditorKit.EndOfLineStringProperty, "\n");
            }
        }
    }*/
}
