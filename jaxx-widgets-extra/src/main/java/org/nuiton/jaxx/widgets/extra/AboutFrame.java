/*
 * #%L
 * JAXX :: Extra Widgets
 * %%
 * Copyright (C) 2004 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.widgets.extra;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.util.Resource;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

import static org.nuiton.i18n.I18n.t;

/**
 * About windows.
 *
 * Can't set :
 * - top image
 * - about tab
 * - license tab (optionnal)
 *
 * @author chatellier
 * @version $Revision$
 *
 *          Last update : $Date$ By : $Author$
 */
public class AboutFrame extends JFrame implements ActionListener,
        HyperlinkListener {

    /** serialVersionUID */
    private static final long serialVersionUID = -8693584537185015506L;
    /** log */
    private static final Log log = LogFactory.getLog(AboutFrame.class);
    /** Top image path (classpath) */
    protected String iconPath;
    /** Background color */
    protected Color backgroundColor;
    /** Html text displayed in about tab */
    protected String aboutHtmlText = "";
    /** License text displayed in license tab */
    protected String licenseText;

    /**
     * Constructor.
     *
     * Build UI.
     */
    public AboutFrame() {
        super();

        // fix resizing
        this.setResizable(false);
    }

    /**
     * Set about text.
     *
     * @param aboutHtmlText the aboutHtmlText to set
     */
    public void setAboutHtmlText(String aboutHtmlText) {
        this.aboutHtmlText = aboutHtmlText;
    }

    /**
     * Set licence text.
     *
     * @param licenseText the licenseText to set
     */
    public void setLicenseText(String licenseText) {
        this.licenseText = licenseText;
    }

    /**
     * Set icon path.
     *
     * @param iconPath the iconPath to set
     */
    public void setIconPath(String iconPath) {
        this.iconPath = iconPath;
    }

    /**
     * Set background color.
     *
     * @param backgroundColor the backgroundColor to set
     */
    public void setBackgroundColor(Color backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    /**
     * Build ui.
     */
    protected void buildUI() {

        setLayout(new GridBagLayout());
        Contraintes c = new Contraintes();

        // top panel
        Component top = getTopPanel();
        c.setConstraints(0, 0, 1, 1, 1, 0, Contraintes.HORIZONTAL,
                         Contraintes.CENTER, new Insets(1, 1, 1, 1), 0, 0);
        add(top, c);

        JTabbedPane tabPanel = new JTabbedPane();

        // first panel
        Component firstTab = getAboutTab();
        tabPanel.add(t("nuitonwidgets.aboutframe.about"), firstTab);

        // second panel
        if (licenseText != null) {
            Component secondTab = getLicenseTab();
            tabPanel.add(t("nuitonwidgets.aboutframe.license"), secondTab);
        }

        c.setConstraints(0, 1, 1, 1, 1, 1, Contraintes.BOTH,
                         Contraintes.CENTER, new Insets(0, 5, 5, 5), 0, 0);
        add(tabPanel, c);

        // ok button
        JButton okButton = new JButton(t("nuitonwidgets.aboutframe.ok"));
        okButton.setActionCommand("ok");
        okButton.addActionListener(this);
        c.setConstraints(0, 2, 1, 1, 1, 0, Contraintes.NONE, Contraintes.EAST,
                         new Insets(0, 5, 5, 5), 20, 0);
        add(okButton, c);

        // change back color
        if (backgroundColor != null) {
            getContentPane().setBackground(backgroundColor);
        }
    }

    /**
     * Build the top component
     *
     * @return top component
     */
    protected Component getTopPanel() {
        // image
        JLabel labelIcon;
        if (iconPath != null) {
            Icon logoIcon = Resource.getIcon(iconPath);
            labelIcon = new JLabel(logoIcon);
        } else {
            labelIcon = new JLabel();
        }
        return labelIcon;
    }

    /**
     * Build license tab.
     *
     * @return license tab component
     */
    protected Component getLicenseTab() {

        JTextArea licenseArea = new JTextArea();
        licenseArea.setLineWrap(true);
        licenseArea.setWrapStyleWord(true);
        licenseArea.setText(licenseText);
        licenseArea.setEditable(false);
        return licenseArea;
    }

    /**
     * Build about tab.
     *
     * @return about tab component
     */
    protected Component getAboutTab() {

        JEditorPane htmlAbout = new JEditorPane("text/html", aboutHtmlText);
        htmlAbout.addHyperlinkListener(this);
        htmlAbout.setEditable(false);

        return htmlAbout;
    }

    /*
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        String command = e.getActionCommand();

        if ("ok".equals(command)) {
            setVisible(false);
        }
    }

    /*
     * @see javax.swing.event.HyperlinkListener#hyperlinkUpdate(javax.swing.event.HyperlinkEvent)
     */
    @Override
    public void hyperlinkUpdate(HyperlinkEvent he) {
        if (he.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {

            if (Desktop.isDesktopSupported()) {
                try {
                    URL u = he.getURL();
                    if (u.getProtocol().equalsIgnoreCase("mailto") || u.getProtocol().equalsIgnoreCase("http") || u.getProtocol().equalsIgnoreCase("ftp")) {
                        Desktop.getDesktop().browse(u.toURI());
                    }
                } catch (IOException | URISyntaxException e) {
                    if (log.isErrorEnabled()) {
                        log.error("Error while opening link", e);
                    }
                }
            }
        }
    }

    @Override
    public void setVisible(boolean b) {
        if (b) {
            // build UI
            buildUI();
        }

        super.setVisible(b);
    }
}

class Contraintes extends GridBagConstraints {

    /** serialVersionUID. */
    public static final long serialVersionUID = 77885838537048102L;

    /**
     * Modifie les contraintes.
     *
     * @param x          Num&eacute;ro de cellule en abscisse
     * @param y          Num&eacute;ro de cellule en ordonn&eacute;e
     * @param nbx        Nombre de cellules occup&eacute;es en abscisse
     * @param nby        Nombre de cellules occup&eacute;es en ordonn&eacute;e
     * @param wx         Proportion en abscisse
     * @param wy         Proportion en ordonn&eacute;e
     * @param fill       M&eacute;thode de remplissage de la cellule
     * @param anchor     Position dans la cellule
     * @param espacement <tt>Insets</tt> sp&eacute;cifiant les espaces
     *                   lat&eacute;raux ( <tt>new Insets(haut,gauche,bas,droite)</tt>)
     * @param etirx      Etrirement de la cellule en abscisse
     * @param etiry      Etrirement de la cellule en ordonn&eacute;e
     */
    public void setConstraints(int x, int y, int nbx, int nby, int wx, int wy,
                               int fill, int anchor, Insets espacement, int etirx, int etiry) {
        this.gridx = x;
        this.gridy = y;
        this.gridwidth = nbx;
        this.gridheight = nby;
        this.weightx = wx;
        this.weighty = wy;
        this.fill = fill;
        this.anchor = anchor;
        this.insets = espacement;
        this.ipadx = etirx;
        this.ipady = etiry;
    }
}
