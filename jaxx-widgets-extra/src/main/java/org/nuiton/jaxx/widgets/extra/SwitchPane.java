/*
 * #%L
 * JAXX :: Extra Widgets
 * %%
 * Copyright (C) 2004 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/*
 * Created on 16 mai 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.nuiton.jaxx.widgets.extra;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.LayoutManager;

/**
 * @author cedric
 *
 *         TODO To change the template for this generated type comment go to
 *         Window - Preferences - Java - Code Style - Code Templates
 */
public class SwitchPane extends JPanel {

    private static final long serialVersionUID = 1L;

    /**
     * Constructeur unique pour le SwitchPane
     */
    public SwitchPane() {
        super();
    }

    @Override
    public void setLayout(LayoutManager mgr) {
        super.setLayout(new BorderLayout());
    }

    public void switchTo(Component component) {
        super.removeAll();
        super.add(component, BorderLayout.CENTER);
        revalidate();
        repaint();
    }

    public SwitchPane(LayoutManager layout, boolean isDoubleBuffered) {
        throw new UnsupportedOperationException();
    }

    public SwitchPane(LayoutManager layout) {
        throw new UnsupportedOperationException();
    }

    public SwitchPane(boolean isDoubleBuffered) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Component add(Component component) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Component add(Component component, int index) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void add(Component component, Object constraints) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void add(Component component, Object constraints, int index) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Component add(String name, Component component) {
        throw new UnsupportedOperationException();
    }

}
