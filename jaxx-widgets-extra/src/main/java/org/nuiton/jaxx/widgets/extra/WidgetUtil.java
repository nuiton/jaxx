/*
 * #%L
 * JAXX :: Extra Widgets
 * %%
 * Copyright (C) 2004 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/* *
 * WidgetUtil.java
 *
 * Created: Jul 26, 2004
 *
 * @author Benjamin Poussin - poussin@codelutin.com
 * Copyright Code Lutin
 * @version $Revision$
 *
 * Mise a jour: $Date$
 * par : $Author$
 */

package org.nuiton.jaxx.widgets.extra;

import java.awt.Component;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

public class WidgetUtil { // WidgetUtil

    public static Component makeDeepCopy(Component clone) {
        XMLEncoder e = null;
        XMLDecoder d = null;
        try {

            ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
            e = new XMLEncoder(byteOut);
            e.writeObject(clone);
            e.flush();

            ByteArrayInputStream byteIn = new ByteArrayInputStream(byteOut
                                                                           .toByteArray());
            d = new XMLDecoder(byteIn);

            return (Component) d.readObject();

        } catch (Exception eee) {
            eee.printStackTrace();
            throw (eee);
        } finally {
            if (e != null) {
                e.close();
            }
            if (d != null) {
                d.close();
            }
        }

    }

} // WidgetUtil

