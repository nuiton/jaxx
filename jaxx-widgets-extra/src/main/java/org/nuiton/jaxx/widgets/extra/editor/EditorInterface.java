/*
 * #%L
 * JAXX :: Extra Widgets
 * %%
 * Copyright (C) 2004 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.widgets.extra.editor;

import javax.swing.event.CaretListener;
import javax.swing.event.DocumentListener;
import java.io.File;

/**
 * EditorInterface.
 *
 * @author poussin
 *
 *         Created: 6 août 2006 12:54:19
 * @author poussin
 * @version $Revision$
 *
 *          Last update: $Date$
 *          by : $Author$
 */
public interface EditorInterface {

    /**
     * Add document listener.
     *
     * @param listener listener
     */
    void addDocumentListener(DocumentListener listener);

    /**
     * Remove document listener.
     *
     * @param listener listener
     */
    void removeDocumentListener(DocumentListener listener);

    /**
     * Add caret listener.
     *
     * @param listener listener
     */
    void addCaretListener(CaretListener listener);

    /**
     * Remove caret listener.
     *
     * @param listener listener
     */
    void removeCaretListener(CaretListener listener);

    /**
     * If return true, this editor support this file type.
     * Default implementation return {@code true}.
     *
     * @param file file to test
     * @return if return {@code true}, this editor support this file type.
     */
    boolean accept(File file);

    /**
     * If return true, this editor support the syntax type.
     * Default implementation return {@code true}.
     *
     * @param editorSyntaxConstant syntaxe type to test
     * @return if return {@code true}, this editor support this syntax type.
     */
    boolean accept(Editor.EditorSyntaxConstant editorSyntaxConstant);

    /**
     * Indicate if current opened file has been modified.
     *
     * @return {@code true} if current file is modified
     */
    boolean isModified();

    /**
     * Replace the current edited file by file passed in argument.
     *
     * @param file the file to open
     * @return true if file has been opened
     */
    boolean open(File file);

    /**
     * Replace the current edited file by file passed in argument.
     *
     * @param file the file to open
     * @return true if file has been saved and reopen with new name
     */
    boolean saveAs(File file);

    /**
     * Return the current content text of the editor as {@link String}.
     *
     * @return return the current content text of the editor as {@link String}
     */
    String getText();

    /**
     * Set all text with text in argument.
     *
     * @param text test to set
     */
    void setText(String text);

    /**
     * Cut current editor selection into system clipboard.
     */
    void cut();

    /**
     * Copy current current selection into system clipboard.
     */
    void copy();

    /**
     * Paste current clicboard content into editor at caret position.
     */
    void paste();

    /**
     * Enable/disable editor.
     *
     * @param b enable
     */
    void setEnabled(boolean b);

    /**
     * Force syntax to use
     *
     * @param editorSyntax to use
     */
    void setSyntax(Editor.EditorSyntaxConstant editorSyntax);
}
