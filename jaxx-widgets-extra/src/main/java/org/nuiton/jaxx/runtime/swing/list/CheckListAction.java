package org.nuiton.jaxx.runtime.swing.list;

/*
 * #%L
 * JAXX :: Extra Widgets
 * %%
 * Copyright (C) 2008 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


import java.util.ArrayList;
import java.util.Collection;

import static org.nuiton.i18n.I18n.t;


public interface CheckListAction<T> {

    void check(CheckListModel<T> model, boolean value);

    class CheckAll<T> implements CheckListAction<T> {

        @Override
        public String toString() {
            return "(" + t("jaxx.list.check.all") + ")";
        }

        @SuppressWarnings("unchecked")
        @Override
        public void check(CheckListModel<T> model, boolean value) {
            Collection<T> items = new ArrayList<>();
            if (value) {
                for (int i = 0, s = model.getSize(); i < s; i++) {
                    items.add((T) model.getElementAt(i));
                }
            }
            model.setCheckedItems(items);

        }

    }

}
