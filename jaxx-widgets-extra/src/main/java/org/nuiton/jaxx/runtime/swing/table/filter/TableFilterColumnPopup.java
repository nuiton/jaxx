/*
 * Copyright (c) 2009-2011, EzWare
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.Redistributions
 * in binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.Neither the name of the
 * EzWare nor the names of its contributors may be used to endorse or
 * promote products derived from this software without specific prior
 * written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * %%Ignore-License%%
 */


package org.nuiton.jaxx.runtime.swing.table.filter;

import org.apache.commons.collections4.CollectionUtils;
import org.nuiton.decorator.Decorator;
import org.nuiton.jaxx.runtime.swing.SwingUtil;
import org.nuiton.jaxx.runtime.swing.JSearchTextField;
import org.nuiton.jaxx.runtime.swing.list.filter.CheckListFilterType;
import org.nuiton.jaxx.runtime.swing.list.filter.DefaultFilterableCheckListModel;
import org.nuiton.jaxx.runtime.swing.list.filter.FilterableActionCheckListModel;
import org.nuiton.jaxx.runtime.swing.list.filter.FilterableCheckList;
import org.nuiton.jaxx.runtime.swing.list.filter.FilterableCheckListModel;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumnModel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static org.nuiton.i18n.I18n.t;

class TableFilterColumnPopup extends MouseAdapter {

    static class ColumnAttrs {
        public Dimension preferredSize;
    }

    private boolean enabled = false;

    private final FilterableCheckList<Object> filterList = new FilterableCheckList();
    private final JSearchTextField searchField = new JSearchTextField();

    private final Map<Integer, ColumnAttrs> colAttrs = new HashMap<>();
    private int mColumnIndex = -1;

    private final TableFilter<?> filter;
    private boolean searchable;
    private Decorator<Object> decorator;
    private boolean actionsVisible = true;
    private boolean useTableRenderers = false;

    private final JPopupMenu menu;

    private Dimension defaultSize = null;

    public TableFilterColumnPopup(TableFilter<?> filter) {

        menu = new ResizablePopupMenu() {

            private static final long serialVersionUID = 1L;

            @Override
            public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
                if (menu.getComponentCount() == 0) {
                    JComponent content = buildContent();
                    if (defaultSize == null) {
                        defaultSize = content.getPreferredSize();
                    } else {
                        content.setPreferredSize(defaultSize);
                    }

                    menu.add(content);

                }
                beforeShow();
            }

            @Override
            public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
                beforeHide();
            }

        };

        this.filter = filter;
        filterList.getList().setVisibleRowCount(8);

        setupTableHeader();
        filter.getTable().addPropertyChangeListener("tableHeader", evt -> setupTableHeader()
        );
        filter.getTable().addPropertyChangeListener("model", evt -> colAttrs.clear()
        );

        searchField.getDocument().addDocumentListener(new DocumentListener() {

            @Override
            public void removeUpdate(DocumentEvent e) {
                perform(e);
            }

            @Override
            public void insertUpdate(DocumentEvent e) {
                perform(e);
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                perform(e);
            }

            private void perform(DocumentEvent e) {
                String text = searchField.getText();

                //#3601 [TABLE FILTER] uncheck the "all" action when the user starts typing in the search field
                if (actionsVisible) {
                    FilterableCheckListModel<Object> model = filterList.getModel();
                    if (model.isCheckedIndex(0)) {
                        model.setCheckedIndex(0, false);
                    }

                }
                filterList.filter(text, decorator, CheckListFilterType.CONTAINS);
            }

        });

    }

    public final Dimension getDefaultSize() {
        return defaultSize;
    }

    public final void setDefaultSize(Dimension dimension) {
        defaultSize = dimension;
    }

    public final Dimension getPreferredSize() {
        return menu.getPreferredSize();
    }

    public final void setPreferredSize(Dimension preferredSize) {
        menu.setPreferredSize(preferredSize);
    }

    /**
     * Shows Popup in predefined location
     *
     * @param invoker
     * @param x
     * @param y
     */
    public void show(Component invoker, int x, int y) {
        menu.show(invoker, x, y);
    }

    /**
     * Shows popup in predefined location
     *
     * @param invoker
     * @param location
     */
    public void show(Component invoker, Point location) {
        show(invoker, location.x, location.y);
    }

    /**
     * Hides popup
     */
    public final void hide() {
        menu.setVisible(false);
    }

    public void setSearchable(boolean searchable) {
        this.searchable = searchable;
    }

    public void searchDecorator(Decorator<Object> decorator) {
        this.decorator = decorator;
    }

    public void setActionsVisible(boolean actionsVisible) {
        this.actionsVisible = actionsVisible;
    }

    public void setUseTableRenderers(boolean reuseRenderers) {
        this.useTableRenderers = reuseRenderers;
    }

    private void setupTableHeader() {
        JTableHeader header = filter.getTable().getTableHeader();
        if (header != null) header.addMouseListener(this);
    }

    protected JComponent buildContent() {

        JPanel owner = new JPanel(new BorderLayout(3, 3));
        owner.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        owner.setPreferredSize(new Dimension(250, 150)); // default popup size

        Box commands = new Box(BoxLayout.LINE_AXIS);

        JToolBar toolbar = new JToolBar();
        toolbar.setFloatable(false);
        toolbar.setOpaque(false);
        toolbar.add(new CommandAction(
                t("jaxx.table.filter.popup.button.clearAll"),
                SwingUtil.createImageIcon("funnel_delete.png")) {
            @Override
            protected boolean perform() {
                return clearAllFilters();
            }
        });
        commands.add(toolbar);

        commands.add(Box.createHorizontalGlue());

        commands.add(new JButton(new CommandAction(t("jaxx.table.filter.popup.button.apply")) {
                         @Override
                         protected boolean perform() {
                             return applyColumnFilter();
                         }
                     })
        );
        commands.add(Box.createHorizontalStrut(5));
        commands.add(new JButton(new CommandAction(t("jaxx.table.filter.popup.button.cancel"))));
        commands.setBorder(BorderFactory.createEmptyBorder(3, 0, 3, 0));
        commands.setBackground(UIManager.getColor("Panel.background"));
        commands.setOpaque(true);

        if (searchable) {
            owner.add(searchField, BorderLayout.NORTH);
        }
        owner.add(new JScrollPane(filterList.getList()), BorderLayout.CENTER);
        owner.add(commands, BorderLayout.SOUTH);

        return owner;

    }

    private boolean applyColumnFilter() {
        Collection<Object> checked = filterList.getCheckedItems();
        FilterableCheckListModel<Object> model = filterList.getModel();
        model.filter("", decorator, CheckListFilterType.CONTAINS); // clear filter to get true results
        filter.apply(mColumnIndex, checked);
        return true;
    }

    private boolean clearAllFilters() {
        filter.clear();
        return true;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    // Popup menus are triggered differently on different platforms
    // Therefore, isPopupTrigger should be checked in both mousePressed and mouseReleased
    // events for for proper cross-platform functionality

    @Override
    public void mousePressed(MouseEvent e) {
        if (enabled && e.isPopupTrigger()) showFilterPopup(e);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if (enabled && e.isPopupTrigger()) showFilterPopup(e);
    }

    private void showFilterPopup(MouseEvent e) {
        JTableHeader header = (JTableHeader) (e.getSource());
        TableColumnModel colModel = filter.getTable().getColumnModel();

        // The index of the column whose header was clicked
        int vColumnIndex = colModel.getColumnIndexAtX(e.getX());
        if (vColumnIndex < 0) return;


        // Determine if mouse was clicked between column heads
        Rectangle headerRect = filter.getTable().getTableHeader().getHeaderRect(vColumnIndex);
        if (vColumnIndex == 0) {
            headerRect.width -= 2;
        } else {
            headerRect.grow(-2, 0);
        }

        // Mouse was clicked between column heads
        if (!headerRect.contains(e.getX(), e.getY())) return;

        // restore popup's size for the column
        mColumnIndex = filter.getTable().convertColumnIndexToModel(vColumnIndex);
        setPreferredSize(getColumnAttrs(vColumnIndex).preferredSize);

        Collection<Object> distinctItems = filter.getDistinctColumnItems(mColumnIndex);
        if (distinctItems != null) {

            DefaultFilterableCheckListModel<Object> model = new DefaultFilterableCheckListModel<>(distinctItems);
            filterList.setModel(actionsVisible ? new FilterableActionCheckListModel<>(model) : model);
            Collection<Object> checked = filter.getFilterState(mColumnIndex);

            // replace empty checked items with full selection
            filterList.setCheckedItems(CollectionUtils.isEmpty(checked) ? distinctItems : checked);

            if (useTableRenderers) {
                filterList.getList().setCellRenderer(new TableAwareCheckListRenderer(decorator));
            }

            // show pop-up
            show(header, headerRect.x, header.getHeight());
        }
    }

    private ColumnAttrs getColumnAttrs(int column) {
        ColumnAttrs attrs = colAttrs.computeIfAbsent(column, k -> new ColumnAttrs());

        return attrs;
    }

    protected void beforeShow() {
        if (searchable) {
            SwingUtilities.invokeLater(() -> {
                searchField.setText("");
                searchField.requestFocusInWindow();
            });
        }
    }

    public void beforeHide() {
        // save pop-up's dimensions before pop-up becomes hidden
        getColumnAttrs(mColumnIndex).preferredSize = getPreferredSize();
    }

    /**
     * Simple action to for the popup window.
     * To use - override perform method.
     *
     * Created on Feb 4, 2011
     *
     * @author Eugene Ryzhikov
     */
    protected class CommandAction extends AbstractAction {

        private static final long serialVersionUID = 1L;

        public CommandAction(String name, Icon icon) {
            super(name, icon);

            if (icon != null) {
                putValue(Action.SHORT_DESCRIPTION, name);
                putValue(Action.NAME, null);
            }

        }

        public CommandAction(String name) {
            super(name);
        }

        @Override
        public final void actionPerformed(ActionEvent e) {
            if (perform()) hide();
        }

        /**
         * Preforms action
         *
         * @return true if popup should be closed
         */
        protected boolean perform() {
            return true;
        }
    }

    protected class ResizablePopupMenu extends JPopupMenu implements PopupMenuListener {

        private static final long serialVersionUID = 1L;

        private static final int DOT_SIZE = 2;
        private static final int DOT_START = 2;
        private static final int DOT_STEP = 4;

        public ResizablePopupMenu() {
            super();
            new PopupMenuResizer(this);
            addPopupMenuListener(this);
        }

        @Override
        public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
        }

        @Override
        public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
        }

        @Override
        public void popupMenuCanceled(PopupMenuEvent e) {
        }

        @Override
        public void paintChildren(Graphics g) {
            super.paintChildren(g);
            drawResizer(g);
        }

        private void drawResizer(Graphics g) {

            int x = getWidth() - 2;
            int y = getHeight() - 2;

            Graphics g2 = g.create();

            try {
                for (int dy = DOT_START, j = 2; j > 0; j--, dy += DOT_STEP) {
                    for (int dx = DOT_START, i = 0; i < j; i++, dx += DOT_STEP) {
                        drawDot(g2, x - dx, y - dy);
                    }
                }

            } finally {
                g2.dispose();
            }
        }

        private void drawDot(Graphics g, int x, int y) {
            g.setColor(Color.WHITE);
            g.fillRect(x, y, DOT_SIZE, DOT_SIZE);
            g.setColor(Color.LIGHT_GRAY);
            g.fillRect(x - 1, y - 1, DOT_SIZE, DOT_SIZE);
        }

    }

    /**
     * Allows to resize popup with the mouse.
     *
     * Created on Aug 6, 2010
     *
     * @author exr0bs5
     */
    protected class PopupMenuResizer extends MouseAdapter {

        private final JPopupMenu menu;

        private static final int REZSIZE_SPOT_SIZE = 10;

        private Point mouseStart = new Point(Integer.MIN_VALUE, Integer.MIN_VALUE);

        private Dimension startSize;

        private boolean isResizing = false;

        public PopupMenuResizer(JPopupMenu menu) {
            this.menu = menu;
            this.menu.setLightWeightPopupEnabled(true);
            menu.addMouseListener(this);
            menu.addMouseMotionListener(this);
        }

        private boolean isInResizeSpot(Point point) {

            if (point == null) return false;

            Rectangle resizeSpot = new Rectangle(
                    menu.getWidth() - REZSIZE_SPOT_SIZE,
                    menu.getHeight() - REZSIZE_SPOT_SIZE,
                    REZSIZE_SPOT_SIZE,
                    REZSIZE_SPOT_SIZE);

            return resizeSpot.contains(point);

        }

        @Override
        public void mouseMoved(MouseEvent e) {

            menu.setCursor(
                    Cursor.getPredefinedCursor(
                            isInResizeSpot(e.getPoint()) ? Cursor.SE_RESIZE_CURSOR : Cursor.DEFAULT_CURSOR));
        }

        private Point toScreen(MouseEvent e) {

            Point p = e.getPoint();
            SwingUtilities.convertPointToScreen(p, e.getComponent());
            return p;

        }

        @Override
        public void mousePressed(MouseEvent e) {
            mouseStart = toScreen(e);
            startSize = menu.getSize();
            isResizing = isInResizeSpot(e.getPoint());
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            mouseStart = new Point(Integer.MIN_VALUE, Integer.MIN_VALUE);
            isResizing = false;
        }

        @Override
        public void mouseDragged(MouseEvent e) {

            if (!isResizing) return;

            Point p = toScreen(e);

            int dx = p.x - mouseStart.x;
            int dy = p.y - mouseStart.y;


            Dimension minDim = menu.getMinimumSize();
            //		Dimension maxDim = menu.getMaximumSize();
            Dimension newDim = new Dimension(startSize.width + dx, startSize.height + dy);

            if (newDim.width >= minDim.width && newDim.height >= minDim.height /*&&
                 newDim.width <= maxDim.width && newDim.height <= maxDim.height*/) {
                menu.setPopupSize(newDim);
            }

        }
    }

}

