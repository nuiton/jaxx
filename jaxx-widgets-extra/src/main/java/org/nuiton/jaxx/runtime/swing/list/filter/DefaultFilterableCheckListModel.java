package org.nuiton.jaxx.runtime.swing.list.filter;

/*
 * #%L
 * JAXX :: Extra Widgets
 * %%
 * Copyright (C) 2008 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.decorator.Decorator;
import org.nuiton.jaxx.runtime.JAXXUtil;
import org.nuiton.jaxx.runtime.swing.list.DefaultCheckListModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 2.13
 */
public class DefaultFilterableCheckListModel<T> extends DefaultCheckListModel<T> implements FilterableCheckListModel<T> {

    private List<T> filteredData = null;

    public DefaultFilterableCheckListModel(Collection<? extends T> data) {
        super(data);
    }

    public DefaultFilterableCheckListModel(T... data) {
        super(Arrays.asList(data));
    }

    protected List<T> data() {
        return filteredData == null ? data : filteredData;
    }

    @Override
    public void filter(String filter, Decorator<Object> decorator, CheckListFilterType filterType) {

        if (filter == null || filter.trim().length() == 0) {
            filteredData = null;
        } else {

            CheckListFilterType ft = filterType == null ? CheckListFilterType.CONTAINS : filterType;

            String f = filter.toLowerCase();

            List<T> fData = new ArrayList<>();

            for (T o : data) {
                String decorated;
                if (o != null && decorator != null) {
                    decorated = decorator.toString(o);
                } else {
                    decorated = JAXXUtil.getStringValue(o);
                }
                if (ft.include(decorated, f)) {
                    fData.add(o);
                }
            }
            filteredData = fData;
        }

        fireContentsChanged(this, 0, data.size() - 1);
    }

}
