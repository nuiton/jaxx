package org.nuiton.jaxx.runtime.swing.list.filter;

/*
 * #%L
 * JAXX :: Extra Widgets
 * %%
 * Copyright (C) 2008 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.decorator.Decorator;
import org.nuiton.jaxx.runtime.swing.list.CheckListModel;

/**
 * @author Kevin Morin (Code Lutin)
 * @since 2.13
 */
public interface FilterableCheckListModel<T> extends CheckListModel<T> {

    /**
     * Filters list view without losing actual data
     *
     * @param filter
     */
    void filter(String filter, Decorator<Object> decorator, CheckListFilterType filterType);

}
