/*
 * Copyright (c) 2009-2011, EzWare
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.Redistributions
 * in binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.Neither the name of the
 * EzWare nor the names of its contributors may be used to endorse or
 * promote products derived from this software without specific prior
 * written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * %%Ignore-License%%
 */

package org.nuiton.jaxx.runtime.swing.table.filter;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ComparatorUtils;
import org.apache.commons.collections4.Transformer;

import javax.swing.JTable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * Partial implementation of table filter
 *
 * Created on Feb 10, 2011
 *
 * @param <T>
 * @author Eugene Ryzhikov
 * @author Kevin Morin
 */
@SuppressWarnings("serial")
public abstract class AbstractTableFilter<T extends JTable> implements TableFilter<T> {

    private final Set<FilterChangeListener> listeners = Collections.synchronizedSet(new HashSet<FilterChangeListener>());

    private final T table;
    private final TableFilterState filterState = new TableFilterState();
    private final Map<Integer, Integer> columnDistnctIntemNumbers = new HashMap<>();

    public AbstractTableFilter(T table) {
        this.table = table;
    }

    @Override
    public T getTable() {
        return table;
    }

    protected abstract boolean execute(int col, Collection<Object> items);

    @Override
    public boolean apply(int col, Collection<Object> items) {
        setFilterState(col, items);
        boolean result;
        if (result = execute(col, items)) fireFilterChange();
        return result;
    }

    @Override
    public final void addChangeListener(FilterChangeListener listener) {
        if (listener != null) listeners.add(listener);
    }

    @Override
    public final void removeChangeListener(FilterChangeListener listener) {
        if (listener != null) listeners.remove(listener);
    }

    public final void fireFilterChange() {
        for (FilterChangeListener l : listeners) {
            l.filterChanged(AbstractTableFilter.this);
        }
    }

    @Override
    public Collection<Object> getDistinctColumnItems(int column) {
        Collection<Object> result = collectDistinctColumnItems(column);
        columnDistnctIntemNumbers.put(column, result != null ? result.size() : 0);
        return result;

    }

    private Collection<Object> collectDistinctColumnItems(final int column) {
//		Set<Object> set = new HashSet<Object>(); // to collect unique items
//		int nullIndex = -1;
//		for( int row=0; row<table.getModel().getRowCount(); row++) {
//			Object value = table.getModel().getValueAt( row, column);
//			// adding null to TreeSet will produce NPE, just remember we had it
//			if ( value == null ) {
//				nullIndex = row;
//			} else {
//				set.add( new DistinctColumnItem(value, row ));
//			}
//		}
        Set<Object> set = distinctValuesForColumn(column);
        List<Object> result = null;
        if (set != null) {
            result = new ArrayList<>(set);
            //		if ( nullIndex >= 0 ) result.add(0, null); // add null to resulting collection if we had it

            result.sort((o1, o2) -> ComparatorUtils.transformedComparator(ComparatorUtils.NATURAL_COMPARATOR,
                                                                          (Transformer<Object, Comparable>) AbstractTableFilter.this::toString).compare(o1, o2));

        }
        return result;
    }

    @Override
    public Collection<Object> getFilterState(int column) {
        return filterState.getValues(column);
    }

    @Override
    public boolean isFiltered(int column) {
        Collection<Object> checks = getFilterState(column);
        return CollectionUtils.isNotEmpty(checks) && !Objects.equals(columnDistnctIntemNumbers.get(column), checks.size());
    }

    @Override
    public boolean includeRow(TableFilter.Row row) {
        return filterState.include(row);
    }

    public void setFilterState(int column, Collection<Object> values) {
        filterState.setValues(column, values);
    }

    public void clear() {
        filterState.clear();
        fireFilterChange();
    }

}
