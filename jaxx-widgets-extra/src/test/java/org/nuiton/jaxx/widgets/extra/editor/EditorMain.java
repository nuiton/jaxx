/*
 * #%L
 * JAXX :: Extra Widgets
 * %%
 * Copyright (C) 2004 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/* *
 * EditorMain.java
 *
 * Created: 6 août 2006 17:08:26
 *
 * @author poussin
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */

package org.nuiton.jaxx.widgets.extra.editor;

import org.apache.commons.io.FileUtils;

import javax.swing.JFrame;
import javax.swing.WindowConstants;
import java.awt.BorderLayout;
import java.io.File;
import java.nio.charset.StandardCharsets;

/**
 * @author poussin
 */

public class EditorMain {

    public static void main(String[] args) throws Exception {
        JFrame frame = new JFrame("Editor");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(new BorderLayout());
        Editor editor = new Editor();
        frame.getContentPane().add(editor);
        frame.setBounds(10, 10, 300, 300);

        File file = File.createTempFile("NuitonEditorTest", ".java");
        FileUtils.writeStringToFile(file, "public class toto {\n  public void test() {\n    \n  }\n}", StandardCharsets.UTF_8);
        file.deleteOnExit();

        editor.open(file);

        frame.setVisible(true);
    }

}
