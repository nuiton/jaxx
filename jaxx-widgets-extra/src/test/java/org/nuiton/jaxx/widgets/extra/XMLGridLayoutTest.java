/*
 * #%L
 * JAXX :: Extra Widgets
 * %%
 * Copyright (C) 2004 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/* *
 * XMLGridLayoutTest.java
 *
 * Created: 3 aout 2005 18:12:37 CEST
 *
 * @author Benjamin POUSSIN <poussin@codelutin.com>
 * @version $Revision$
 *
 * Last update: $Date$
 * by : $Author$
 */

package org.nuiton.jaxx.widgets.extra;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assume;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.GraphicsEnvironment;

public class XMLGridLayoutTest { // XMLGridLayoutTest

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(XMLGridLayoutTest.class);

    @BeforeClass
    public static void setUpClass() throws Exception {

        Assume.assumeTrue("Can't start test with headless env", !GraphicsEnvironment.isHeadless());

    }

    @Test
    public void testString() throws Exception {
        JPanel panel = new JPanel(
                new XMLGridLayout(
                        "<table><tr><td>button2</td></tr><tr><td>button1</td></tr></table>"));
        panel.add("button1", new JButton("Button1"));
        panel.add("button2", new JButton("Button2"));
        panel.add("button3", new JButton("Button3"));

        JFrame f = new JFrame("testString");
        f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        f.getContentPane().add(panel);
        f.pack();
        f.setVisible(true);
    }

    public void testFile() throws Exception {
        // JPanel panel = new JPanel(new
        // XMLGridLayout("org.nuiton.widget/XMLGridLayoutTest.xgl"));
        // panel.add("button1", new JButton("Button1"));
        // panel.add("button2", new JButton("Button2"));
        // panel.add("button3", new JButton("Button3"));
        //
        // JFrame f = new JFrame("testFile");
        // f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        // f.getContentPane().add(panel);
        // f.pack();
        // f.setVisible(true);
    }

} // XMLGridLayoutTest

