/*
 * #%L
 * JAXX :: Extra Widgets
 * %%
 * Copyright (C) 2004 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.jaxx.widgets.extra.toolTip;

import org.nuiton.i18n.I18n;
import org.nuiton.jaxx.widgets.extra.tooltip.FocusableTip;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.WindowConstants;
import java.awt.BorderLayout;
import java.awt.event.MouseEvent;
import java.util.Locale;

/**
 * @author sletellier
 */
public class FocusableToolTipMain {

    public static void main(String[] args) throws Exception {

        // init i18n
        I18n.init(null, Locale.FRANCE);

        JFrame frame = new JFrame("FocusableToolTip");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(new BorderLayout());

        JLabel simpleToolTipLabel = new JLabel("Simple tool tip text") {
            protected FocusableTip focusableTip;

            @Override
            public String getToolTipText(MouseEvent event) {
                String toolTipText = super.getToolTipText(event);

                // display it into persistant tooltip
                if (focusableTip == null) {
                    focusableTip = new FocusableTip(this, true);
                    focusableTip.setSize(600, 400);
                }
                focusableTip.toolTipRequested(event, toolTipText);

                return null;
            }
        };
        simpleToolTipLabel.setToolTipText("Simple tool tip text<p><b>is Working !</b>");
        frame.getContentPane().add(simpleToolTipLabel, BorderLayout.NORTH);

        JLabel complexeToolTipLabel = new JLabel("Complexe tool tip text") {
            protected FocusableTip focusableTip;

            @Override
            public String getToolTipText(MouseEvent event) {
                super.getToolTipText(event);

                // display it into persistant tooltip
                if (focusableTip == null) {
                    focusableTip = new FocusableTip(this, true);
                    focusableTip.setSize(600, 400);
                }
                focusableTip.toolTipRequested(event, new JTree());

                return null;
            }
        };
        complexeToolTipLabel.setToolTipText(FocusableTip.DUMMY_TOOL_TIP);
        frame.getContentPane().add(complexeToolTipLabel, BorderLayout.CENTER);

        JLabel emptyToolTipLabel = new JLabel("Empty tool tip text") {
            protected FocusableTip focusableTip;

            @Override
            public String getToolTipText(MouseEvent event) {
                String toolTipText = super.getToolTipText(event);

                // display it into persistant tooltip
                if (focusableTip == null) {
                    focusableTip = new FocusableTip(this, true);
                    focusableTip.setSize(600, 400);
                }
                focusableTip.toolTipRequested(event, toolTipText);

                return null;
            }
        };
        emptyToolTipLabel.setToolTipText(null);
        frame.getContentPane().add(emptyToolTipLabel, BorderLayout.SOUTH);

        frame.setVisible(true);
        frame.pack();
    }
}
