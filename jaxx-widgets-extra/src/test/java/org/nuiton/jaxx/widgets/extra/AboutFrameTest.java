/*
 * #%L
 * JAXX :: Extra Widgets
 * %%
 * Copyright (C) 2004 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.jaxx.widgets.extra;

import org.junit.Assume;
import org.junit.BeforeClass;
import org.junit.Test;

import java.awt.GraphicsEnvironment;

/**
 * Test class for about frame.
 *
 * @author chatellier
 * @version $Revision$
 *
 *          Last update : $Date$
 *          By : $Author$
 */
public class AboutFrameTest {

    @BeforeClass
    public static void setUpClass() throws Exception {

        Assume.assumeTrue("Can't start test with headless env", !GraphicsEnvironment.isHeadless());

    }

    /**
     * Affiche une about frame avec
     * seuelement l'onglet about.
     */
    @Test
    public void testOnlyAbout() {
        AboutFrame about = new AboutFrame();

        about.setAboutHtmlText("test");

        about.setVisible(true);
    }

    /**
     * Affiche une about frame avec
     * l'onglet about et licence.
     */
    @Test
    public void testAboutAndLicense() {
        AboutFrame about = new AboutFrame();

        about.setAboutHtmlText("test");
        about.setLicenseText("licence");

        about.setVisible(true);
    }

    /**
     * Affiche une about frame avec
     * l'onglet about et licence.
     */
    @Test
    public void testIcon() {
        AboutFrame about = new AboutFrame();

        about.setIconPath("undo.png");

        about.setVisible(true);
    }
}
