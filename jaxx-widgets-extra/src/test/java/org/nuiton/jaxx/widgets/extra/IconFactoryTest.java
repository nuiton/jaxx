/*
 * #%L
 * JAXX :: Extra Widgets
 * %%
 * Copyright (C) 2004 - 2017 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/* *
 * IconFactoryTest.java
 *
 * Created: 12 août 2004
 *
 * @author Benjamin Poussin - poussin@codelutin.com
 * @version $Revision$
 *
 * Mise a jour: $Date$
 * par : $Author$
 */

package org.nuiton.jaxx.widgets.extra;

import org.junit.Assert;
import org.junit.Assume;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.swing.Icon;
import java.awt.GraphicsEnvironment;

public class IconFactoryTest { // IconFactoryTest

    @BeforeClass
    public static void setUpClass() throws Exception {

        Assume.assumeTrue("Can't start test with headless env", !GraphicsEnvironment.isHeadless());

    }

    @Test
    public void testGetIcon() throws Exception {
        Icon icon0 = IconFactory.getIcon(IconFactory.UNDO);
        Icon icon1 = IconFactory.getIcon(IconFactory.UNDO);

        Assert.assertEquals(icon0, icon1);

        try {
            IconFactory.getIcon("ergjlk");
            Assert.assertTrue(false);
        } catch (IllegalArgumentException eee) {
            Assert.assertTrue(true);
        }

        // test supprime, car le IdentityMap, ne semble pas faire de distinction
        // sur les chaines de caractere :(
        // try{
        // IconFactory.getIcon("UNDO");
        // assertTrue(false);
        // }catch(IllegalArgumentException eee){
        // assertTrue(true);
        // }
    }

} // IconFactoryTest

