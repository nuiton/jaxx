1.5
  * 20090506 [chemit] - super-pom has no dependencies

1.3 chemit 20090409
  * 20090404 [chemit] - introduce DataContext class
  * 20090331 [chemit] - introduce DecoratorUtils class
  * 20090302 [chemit] - add pcs in ApplicationContext
                      - add method in Util to filter JAXX property changed listeners

1.2 letellier 2009022?
  * 2009021 [chemit] - introduce DefaultApplicationContext iwth annotation system.

1.1 chemit 20090220
  * 20090124 [chemit] - add methods to retreave icons from UIManager in Util class
  * 20090123 [chemit] - add a simple createIcon method in Util classe (to create an icon with no path prefix)
                      - add a UIManager key "default.icon.path" to be able to change the path where to find icons
  * 20090122 [chemit] - refactor poms (sibling dependencies, pluginsManagment,...)

1.0 chemit 20090111
  * 20090111 [chemit] - integrate new architecture to allow to have runtime code with NO link with compiler :)

0.8 ??? 200812??
  * 20081228 [chemit] - generify ClassDescriptor
                      - introduce StylesheetHelper helper class to detach Stylesheet, Rule and Selector classes from
                        JAXXCompiler and make possible to extract compiler engine from runtime
                      
  * 20081227 [chemit] - add PCS on ValidatorErrorTable to be used by table validation
  * 20081218 [chemit] - improve generation of methods
  * 20081214 [chemit] - can now in validation, put error with args (all args must be separated by a ##)
                      - improve event naming : replace the $evXXX by doMEthodName__on__field  (except with optimize option)
                      - add jaww.runtime.swing.Utils.fillComboBox to fill a combobox model from a collection
                      - add addSourcesToClassPath property to add sources directories in class-path
                      - improve classloader managment
                      - keep in DataSource objetCode
                      - fix bug when processDataBinding on a null objectCode
                      - always clean demoNode cached values when selected it
                      - add usefull databinding method in Util

  * 20081213 [chemit] - improve navigation tree demoNode rendering with some caches
                      - introduce a ChildBuilder to simplify building of child nodes from a collection or array
  
0.7 chemit 20081210
  * 20081210 [chemit]  - fix bug 1751
  * 20081210 [chemit]  - improve JAXXButtonGroup (add ActionChangeListener and toolTipText mecanism)
  * 20081208 [chemit]  - javabBean attribute use to initialize bean
                      - introduce Base64Coder to fix bug 1750 and control serailVersionUI (put them to 1L for the moment)
                      - introduce MultiJXPathDecorator
                      - add a resetAfterCompile parameter toCompilerOption to keep in test used compilers
                     
  * 20081207 [chemit] use lutinproject 3.1
                      - can exclude field from validator
  * 20081202 [chemit] - add strategy for loading ui in NavigationTreeSelectionAdapter
                      - fix bug when searching for a inner class

  * 20081201 [chemit] - implements jaxx.runtime.JXPathDecorator
                      - add setcontextValue and removeContextValue on JAXXContextEntryDef
                      - introduce scope in BeanValidator (ERROR or WARNING) and related swing stuff
                      - only enter once in $initialize method in generated code

 0.6 chemit 20081117
  * 20081118 [chemit] introduce NavigationUtil, save in context selected demoNode
  * 20081107 [chemit] improve data binding  and code generation :
                      - make possible inheritance in binding
                      - add an attribute javaBean to an object : will generate a full java bean support property
                      - make possible binding to the javaBean added properties
                      - clean generated code

  * 20081105 [chemit] - introduce a CardLayout2 to extends awt CardLayout
                      - introduce a NavigationTreeModel
                      - introduce a Decorator to render Object
                      - propagate constructor JAXXContext(JAXXContext) in JAXXObject generation
                      - begin of rst documentation

  * 20081104 [chemit] can add extra beanInfoSearchPath in SwingInitializer
  * 20081104 [chemit] add jaxxContextImplementorClass in option to make possible use of other JAXXContext implementor.
  * 20081102 [chemit] improve JAXXContext :
                      - introduce a JAXXContextEntryDef to qualify an entry of a JAXXContext
                      - do javadoc in JAXXContext
                      - add logic in DefaultJAXXContext : seek in parent context if entry not found
  * 20081102 [chemit] improve tests :
                      - fix the last failed test from Jaxx original version :)
                      - dumps tests to JUnit4 :)
  * 20081030 [chemit] improve BeanValidator :
                      - add full PropertyChangeEvent java-bean support and a property valid
                      - when remove bean from validator, must remove errors from model
                      - make possible to have a dynamic errorListModel in jaxx files
  * 20081030 [chemit] improve JAXXContext :
                      - fix setContextValue bug when setting twice a same type for a same key
                      - implements a DefaultJAXXContext
                      - use this default implementation with delegate pattern in JAXXObject
  * 20081030 [chemit] add JAXXAction contract to simplify init of ui with JAXXInitialContext
  * 20081027 [chemit] fix bug 1722
  * 20081027 [chemit] add conversion support in validator
  * 20081025 [chemit] improve BeanValidator tag :
                      - add a errorList attribute for set a ErrorListMouseListener on the errorList
                      - add a beanInitializer attribute for set the validator's bean at runtime
                      - add a default errorListModel value 'errors'
  * 20081025 [chemit] introduce JAXXInitialContext to fill JAXXContext at runtime before $initialize() method
  * 20081024 [chemit] fix validator context lost if UI is launched from another thread
 0.5 chemit 20081002
  * 20081017 [chemit] add validator support
  * 20081013 [chemit] can generate logger on jaxx files
  * 20081011 [chemit] improve site
  * 20081011 [chemit] fix bug on JavaFileParser : works again
  * 20081002 [chemit] Using lutinproject 3.0, changing groupId to org.codelutin
  * 20081002 [chemit] use a single module jaxx-core (no more core, runtime and jaxx-swing modules)
  * 20081002 [chemit] Introduce JAXXContext
  * 20081002 [chemit] Fix bug on method creation via scripting
  * 20081002 [chemit] Improve i18n integration (works now also for tabs)
