/*
 * #%L
 * JAXX :: Widgets DateTime
 * %%
 * Copyright (C) 2008 - 2014 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

#title {
  horizontalAlignment:center;
}

#hourModel {
  calendarField:{Calendar.HOUR_OF_DAY};
  value:{handler.getHourModelValue(model.getTime())};
}

#hourEditor {
  model:{hourModel};
  enabled:{isEnabled()};
}

#labelH {
  text:"timeeditor.H";
  horizontalAlignment:center;
}

#minuteModel {
  calendarField:{Calendar.MINUTE};
  value:{handler.getMinuteModelValue(model.getTime())};
}

#minuteEditor {
  model:{minuteModel};
  enabled:{isEnabled()};
}

#sliderHidorToolBar {
  borderPainted:false;
  floatable:false;
  opaque:false;
}

#sliderHidor {
  target:{slider};
  showTip:{t("timeeditor.show.slider")};
  hideTip:{t("timeeditor.hide.slider")};
  targetVisible:{isShowTimeEditorSlider()};
}

#slider {
  font-size: 11;
  paintTicks:true;
  paintLabels:true;
  majorTickSpacing:60;
  minorTickSpacing:30;
  value:{model.getTimeInMinutes()};
  enabled:{isEnabled()};
  model:{new DefaultBoundedRangeModel(0, 1, 0, 60 * 24)};
}
