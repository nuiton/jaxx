<!--
  #%L
  JAXX :: Widgets File
  %%
  Copyright (C) 2008 - 2017 CodeLutin
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as 
  published by the Free Software Foundation, either version 3 of the 
  License, or (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  
  You should have received a copy of the GNU General Lesser Public 
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  -->
<BaseActionPanel layout='{new BorderLayout()}'>
  <import>
    org.nuiton.jaxx.widgets.file.BaseActionPanel

    org.apache.commons.lang3.StringUtils

    javax.swing.JDialog
    java.io.File
  </import>

  <Boolean id='acceptAllFileFilterUsed' javaBean='Boolean.TRUE'/>

  <Boolean id='directoryEnabled' javaBean='Boolean.TRUE'/>

  <Boolean id='fileEnabled' javaBean='Boolean.TRUE'/>

  <String id='startPath' javaBean='null'/>

  <String id='title' javaBean='null'/>

  <String id='exts' javaBean='null'/>

  <String id='extsDescription' javaBean='null'/>

  <!-- show reset property -->
  <Boolean id='showReset' javaBean='false'/>

  <script><![CDATA[
protected File selectedFile;

public void setSelectedFile(File selectedFile) {
    this.selectedFile = selectedFile;
    setStartPath(selectedFile == null ? null : selectedFile.getAbsolutePath());
    fireActionEvent();
}

public void setSelectedFilePath(String startPath) {
    setSelectedFile(startPath == null ? null : new File(startPath));
}

public File getSelectedFile() {
    if (selectedFile == null) {
        if (StringUtils.isNotEmpty(startPath)) {
            selectedFile = new File(startPath);
        }
    }
    return selectedFile;
}

public void setDialogOwner(JDialog dialogOwner) {
  handler.setDialogOwner(dialogOwner);
}

  ]]></script>

  <JToolBar id='toolbar' floatable='false' borderPainted='false' visible='{isShowReset()}'
            constraints='BorderLayout.WEST'>
    <JButton id='resetButton' actionIcon='fileeditor-reset' toolTipText='fileeditor.action.reset.tip' focusable='false'
             focusPainted='false' enabled='{isEnabled()}' onActionPerformed='setSelectedFile(null)'/>
  </JToolBar>

  <JTextField id='pathField' constraints='BorderLayout.CENTER' enabled='{isEnabled()}' text='{getStartPath()}'
              onKeyReleased='setSelectedFilePath(pathField.getText())'/>

  <JButton id='boutonXslLocation' constraints='BorderLayout.EAST' enabled='{isEnabled()}' actionIcon='open'
           onActionPerformed='handler.openLocation()'/>
</BaseActionPanel>
